<?php
return [
    'url_route_on'           => false,
    'view_replace_str'  =>  [
        '__PUBLIC__'=>'static/admin/',
    ],
    'auth_session_key' => 'sdewhknkawkuahj',
    'dispatch_success_tmpl'  => APP_PATH . 'admin' . DS .'view' . DS . 'success.html',
    'dispatch_error_tmpl'    => APP_PATH . 'admin' . DS .'view' . DS . 'error.html',
    'template'               => [
        'type'         => 'Think',
        'view_path'    => '',
        'view_suffix'  => 'php',
        'view_depr'    => DS,
        'tpl_begin'    => '{',
        'tpl_end'      => '}',
        'taglib_begin' => '{',
        'taglib_end'   => '}',
        'layout_on'     =>  true,
        'layout_name'   =>  'layout',
    ],

];
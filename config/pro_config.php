<?php
return [

    // 应用调试模式
    'app_debug'              => false,
    // 应用Trace
    'app_trace'              => false,
    'database' => [
        'type'            => 'mysql',
        'hostname'        => 'localhost',
        'database'        => 'star_site',
        'username'        => 'root',
        'password'        => 'kdF%j&jdJidaw',
        'hostport'        => '3306',
        'charset'         => 'utf8',
        'prefix'          => 'ss_',
        'debug'           => true,
        'deploy'          => 0,
        'rw_separate'     => false,
        'master_num'      => 1,
        'fields_strict'   => true,
        'resultset_type'  => 'array',
        'auto_timestamp'  => 'datetime',
        'datetime_format' => 'Y-m-d H:i:s',
        'sql_explain'     => false,
    ],
    'db_config2' => [
        'type'            => 'mysql',
        'hostname'        => '10.0.1.162',
        'database'        => 'multilotto_500',
        'username'        => 'multilotto',
        'password'        => '123456',
        'hostport'        => '3306',
        'charset'         => 'utf8',
        'prefix'          => '',
        'debug'           => true,
        'deploy'          => 0,
        'rw_separate'     => false,
        'master_num'      => 1,
        'fields_strict'   => true,
        'resultset_type'  => 'array',
        'auto_timestamp'  => 'datetime',
        'datetime_format' => 'Y-m-d H:i:s',
        'sql_explain'     => false,
    ]

];

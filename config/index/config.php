<?php
return [
    'base_link' =>'http://www.lotto-vm.com/',
    'route_config_file'      => ['route'],
    'view_replace_str'  =>  [
        '__PUBLIC__'=>'static/index/',
    ],
    'dispatch_success_tmpl'  => APP_PATH . 'index' . DS .'view' . DS . 'error.html',
    'dispatch_error_tmpl'    => APP_PATH . 'index' . DS .'view' . DS . 'error.html',
    'template'               => [
        'type'         => 'Think',
        'view_path'    => '',
        'view_suffix'  => 'php',
        'view_depr'    => DS,
        'tpl_begin'    => '{',
        'tpl_end'      => '}',
        'taglib_begin' => '{',
        'taglib_end'   => '}',
        'layout_on'     =>  true,
        'layout_name'   =>  'layout',
    ],
];
<?php
namespace app\cron\command;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;

class Task extends Command
{
    protected function configure()
    {
        $this->setName('Task')->setDescription('Here is the remark ');
    }
    protected function execute(Input $input, Output $output)
    {
        $output->writeln('Date Crontab job start...');
        //$this->getGameTypes();
        //$this->getGames();
        //$this->getGamesPayouttables();
        $this->updateGameTypes();
        $output->writeln('Date Crontab job end...');
    }
    private function updateGameTypes(){
        $list = Db::connect('db_config2')->name('gametypes')->select();
        $data = [];
        foreach ($list as $k=>$v){
            $data['gametypeid'] = $v['gametypeid'];
            $data['continent'] = $v['continent'];
            /*$data[$k]['slug'] = $v['slug'];
            $data[$k]['currency'] = $v['currency'];
            $data[$k]['currentjackpot'] = $v['currentjackpot'];
            $data[$k]['nextdraw'] = $v['nextdraw'];
            $data[$k]['cutoffhours'] = $v['cutoffhours'];
            $data[$k]['country'] = $v['country'];
            $data[$k]['numbersdrawn'] = $v['numbersdrawn'];
            $data[$k]['extranumbersdrawn'] = $v['extranumbersdrawn'];
            $data[$k]['numbermin'] = $v['numbermin'];
            $data[$k]['numbermax'] = $v['numbermax'];
            $data[$k]['priceperdraw'] = $v['priceperdraw'];
            $data[$k]['purchaseprice'] = $v['purchaseprice'];
            $data[$k]['isplayable'] = $v['isplayable'];
            $data[$k]['onhold'] = $v['onhold'];
            $data[$k]['settings'] = $v['settings'];
            $data[$k]['websiteurl'] = $v['websiteurl'];
            $data[$k]['create_time'] = date('Y-m-d H:i:s');*/
            echo $v['name'].'\n';
            Db::connect('database')->name('game_types')->where('gametypeid', $v['gametypeid'])->update($data);;
        }

    }
    private function getGameTypes(){
        $list = Db::connect('db_config2')->name('gametypes')->select();
        $data = [];
        foreach ($list as $k=>$v){
            $data[$k]['gametypeid'] = $v['gametypeid'];
            $data[$k]['name'] = $v['name'];
            $data[$k]['slug'] = $v['slug'];
            $data[$k]['currency'] = $v['currency'];
            $data[$k]['currentjackpot'] = $v['currentjackpot'];
            $data[$k]['nextdraw'] = $v['nextdraw'];
            $data[$k]['cutoffhours'] = $v['cutoffhours'];
            $data[$k]['country'] = $v['country'];
            $data[$k]['numbersdrawn'] = $v['numbersdrawn'];
            $data[$k]['extranumbersdrawn'] = $v['extranumbersdrawn'];
            $data[$k]['numbermin'] = $v['numbermin'];
            $data[$k]['numbermax'] = $v['numbermax'];
            $data[$k]['priceperdraw'] = $v['priceperdraw'];
            $data[$k]['purchaseprice'] = $v['purchaseprice'];
            $data[$k]['isplayable'] = $v['isplayable'];
            $data[$k]['onhold'] = $v['onhold'];
            $data[$k]['settings'] = $v['settings'];
            $data[$k]['websiteurl'] = $v['websiteurl'];
            $data[$k]['create_time'] = date('Y-m-d H:i:s');
            echo $v['name'].'\n';
        }
        Db::connect('database')->name('game_types')->insertAll($data);
    }
    private function getGames(){
        $list = Db::connect('db_config2')->name('games')->order('drawid desc')->limit(10000)->select();
        $data = [];
        foreach ($list as $k=>$v){
            $data[$k]['drawid'] = $v['drawid'];
            $data[$k]['gametypeid'] = $v['gametypeid'];
            $data[$k]['drawdate'] = $v['drawdate'];
            $data[$k]['localdrawdatetime'] = $v['localdrawdatetime'];
            $data[$k]['jackpotsize'] = $v['jackpotsize'];
            $data[$k]['jackpotcurrency'] = $v['jackpotcurrency'];
            $data[$k]['number1'] = $v['number1'];
            $data[$k]['number2'] = $v['number2'];
            $data[$k]['number3'] = $v['number3'];
            $data[$k]['number4'] = $v['number4'];
            $data[$k]['number5'] = $v['number5'];
            $data[$k]['number6'] = $v['number6'];
            $data[$k]['number7'] = $v['number7'];
            $data[$k]['number8'] = $v['number8'];
            $data[$k]['extranumber1'] = $v['extranumber1'];
            $data[$k]['extranumber2'] = $v['extranumber2'];
            $data[$k]['extranumber3'] = $v['extranumber3'];
            $data[$k]['extranumber4'] = $v['extranumber4'];
            $data[$k]['bonusnumber1'] = $v['bonusnumber1'];
            $data[$k]['bonusnumber2'] = $v['bonusnumber2'];
            $data[$k]['bonusnumber3'] = $v['bonusnumber3'];
            $data[$k]['refundnumber1'] = $v['refundnumber1'];
            $data[$k]['winningsdistributed'] = $v['winningsdistributed'];
            $data[$k]['verified'] = $v['verified'];
        }
        Db::connect('database')->name('games')->insertAll($data);
    }
    private function getGamesPayouttables(){
        $list = Db::connect('db_config2')->name('games_payouttables')->order('drawid desc')->limit(10000)->select();
        $data = [];
        foreach ($list as $k=>$v){
            $data[$k]['drawid'] = $v['drawid'];
            $data[$k]['numbers'] = $v['numbers'];
            $data[$k]['extranumbers'] = $v['extranumbers'];
            $data[$k]['bonusnumbers'] = $v['bonusnumbers'];
            $data[$k]['refundnumbers'] = $v['refundnumbers'];
            $data[$k]['probability'] = $v['probability'];
            $data[$k]['payout'] = $v['payout'];
            $data[$k]['payoutcurrency'] = $v['payoutcurrency'];
            $data[$k]['sortorder'] = $v['sortorder'];
        }
        Db::connect('database')->name('games_payouttables')->insertAll($data);
    }
}
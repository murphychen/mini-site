<?php

namespace app\admin\controller;


use think\Controller;
use app\admin\model\Sites;
use think\Db;

class ControllerInc extends Controller
{
    public $adminid = 0;
    public $current_site;
    public function __construct()
    {
        parent::__construct();
        $this->getAC();
        $this->checkLogin();
        $this->getSites();
    }


    public function checkLogin()
    {
        $loginData = session(config('auth_session_key'));
        if(!empty($loginData) && isset($loginData['adminid'])) {
            $this->adminid = $loginData['adminid'];
            $this->assign('adminInfo',$loginData);
        } else {
            $this->redirect('/auth/index');
        }
    }

    public function getSites()
    {
       $sites = Sites::where('is_del', 0)->select();
       $this->current_site = $sites[0];
       $this->assign('current_sites', $this->current_site);
       $this->assign('sites', $sites);
    }

    private function getAC()
    {

        $action = request()->action();
        $controller = request()->controller();
        $this->assign('action',strtolower($action));
        $this->assign('controller',strtolower($controller));
    }

    public function createUniquenessId()
    {
        $id = Db::name('uniqueness')->insertGetId(['value'=>1]);
        return $id;
    }


}
<?php

namespace app\admin\controller;
use app\admin\model\Sites as SiteM;
use app\admin\validate\Sites;

class SiteController extends ControllerInc
{
    public function indexAction()
    {

        return $this->fetch('/site/index');
    }

    public function getAjaxListAction()
    {
        $list = SiteM::where(['is_del'=>0])->order('create_time asc')->paginate();
        $list = $list->toArray();
        $data = [];
        $data['code'] = 0;
        $data['msg'] = '';
        $data['count'] = $list['total'];
        $data['data'] = $list['data'];
        return json($data);
    }

    public function delAction()
    {
        $siteId = input('param.siteid',false);
        if($siteId){
            $res = SiteM::where('siteid', $siteId)->update(['is_del' => 1]);;
            if($res) {
                return json(['code'=>0,'msg'=>'Action success!']);
            }
        }
        return json(['code'=>105,'msg'=>'Action failed!']);
    }

    public function editAction()
    {
        $site = new SiteM();
        $siteId = input('param.siteid',false);
        if(request()->isPost()){
            $postArr = input('post.');
            $siteid = input('post.siteid',false);
            if($siteid) {
                $res = $site->validate('Sites.edit')->allowField(true)->save($postArr, ['siteid'=>$siteid]);
            } else {
                $res = $site->validate('Sites.add')->allowField(true)->save($postArr);
            }
            if($res == false) {
                $this->error($site->getError());
            } else {
                $this->success('Action success!','/site/index');
            }
        };
        if ($siteId) {
            $row = SiteM::get($siteId);
            if(empty($row)){
                $this->error('Data does not exist');
            }
            $this->assign('row', $row);
            return $this->fetch('/site/edit');
        } else {
            return $this->fetch('/site/add');
        }
    }
}
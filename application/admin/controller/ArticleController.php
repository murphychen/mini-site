<?php

namespace app\admin\controller;
use app\admin\model\Article AS ArticleM;
use app\admin\validate\Article;
use app\admin\model\GameTypes;
use app\admin\model\Languages;

class ArticleController extends ControllerInc
{
    public function indexAction()
    {
        $search = input('param.');
        $where = [];
        $article = new ArticleM();
        $language = input('param.languageid',false);
        $title = input('param.title',false);
        $language && $where['a.languageid'] = $language;
        $title && $where['a.title'] = ['like','%'.$title.'%'];
        if(!empty($where)) {
            $article->where($where);
        }
        $list = $article
                ->alias('a')
                ->field('a.*,lt.name as lName')
                ->join('__LANGUAGES__ lt', 'lt.languageid = a.languageid', 'left')
                ->order('a.flag_number desc,a.sort_order asc,a.create_time desc')
                ->paginate();

        $page = $list->render();
        $list = $list->toArray();
        $this->assign('list', $list['data']);
        $this->assign('page', $page);
        $this->assign('search', $search);
        $languages = (new Languages())->select();
        $this->assign('languages',$languages);

        return $this->fetch('/article/index');
    }

    public function translationAction()
    {
        $flag = input('param.flag_number',false);
        if(!$flag) {
            $this->error('failed');
        }
        $article = new ArticleM();
            $list = $article
                ->alias('a')
                ->field('a.*,gt.name as gameTypeName,lt.name as lName')
                ->join('__GAME_TYPES__ gt', 'gt.gametypeid = a.gametypeid', 'left')
                ->join('__LANGUAGES__ lt', 'lt.languageid = a.languageid', 'left')
                ->where('flag_number', $flag)
                ->order('a.flag_number desc,a.sort_order asc,a.create_time desc')
                ->paginate();

        $page = $list->render();
        $list = $list->toArray();
        $this->assign('list', $list['data']);
        $this->assign('flag_number', $flag);
        return $this->fetch('/article/translation');
    }
    public function editAction()
    {
        $articleid = input('param.articleid',false);
        $flag_number = input('param.flag_number',false);
        if(request()->isPost()){
            $article = new ArticleM();
            $postArr = input('post.');
            if(isset($postArr['game_type'])){
                $game_type_arr = $postArr['game_type'];
                unset($postArr['game_type']);
            }
            $articleid = input('post.articleid',false);
            if(!empty($game_type_arr)) {
                $gametypeids = '';
                $gametypes = '';
                foreach ($game_type_arr as $k=>$v){
                    if($gametypeids != '') {
                        $gametypeids .= '|' . explode("|",$v)[0];
                        $gametypes .= '|' . explode("|",$v)[1];
                    } else {
                        $gametypeids .= explode("|",$v)[0];
                        $gametypes .= explode("|",$v)[1];
                    }
                }
                $postArr['gametypeids'] = $gametypeids;
                $postArr['gametypes'] = $gametypes;
            }
            if($articleid){
                $result = $this->validate($postArr,'Article.edit');
                if(false === $result){
                    $this->error($result);
                }
                $res = $article->allowField(true)->save($postArr,['articleid',$articleid]);
                if($res) {
                    if ($flag_number) {
                        $this->success('edit action success !', '/article/translation/flag_number/'.$flag_number);
                    } else {
                        $this->success('edit action success !', '/article/index');
                    }

                }else {
                    $this->error('edit action failed !');
                }
            } else {
                if(isset($postArr['file'])) {
                    unset($postArr['file']);
                }
                $result = $this->validate($postArr,'Article.add');
                if(false === $result){
                    $this->error($result);
                }
                $postArr['flag_number'] = $this->createUniquenessId();
                $res = $article->allowField(true)->save($postArr);
                if($res) {
                    $this->success('add action success !', '/article/index');
                }else {
                    $this->error('add action failed !');
                }
            }
        }
        $gameTypes = new GameTypes();
        $gameTypesList = $gameTypes->order('create_time desc')->select();
        $this->assign('gtlist', $gameTypesList);
        $languages = (new Languages())->select();
        $this->assign('languages',$languages);
        if($articleid) {
            $row = ArticleM::get($articleid);
            $row['gametypeidArr'] = explode("|",$row['gametypeids']);
            $this->assign('row', $row);
            return $this->fetch('/article/edit');
        } else {
            return $this->fetch('/article/add');
        }
    }
    public function createTranslationAction()
    {
        $article = new ArticleM();
        $flag_number = input('param.flag_number',false);
        if(request()->isPost()){

            $postArr = input('post.');
            if(isset($postArr['file'])) {
                unset($postArr['file']);
            }
            $result = $this->validate($postArr,'Article.add');
            if(false === $result){
                $this->error($result);
            }
            $res = $article->allowField(true)->save($postArr);
            if($res) {
                $this->success('add action success !', '/article/translation/flag_number/'.$flag_number);
            }else {
                $this->error('add action failed !');
            }
        }
        $gameTypes = new GameTypes();
        $gameTypesList = $gameTypes->order('create_time desc')->select();
        $this->assign('gtlist', $gameTypesList);
        $languages = (new Languages())->select();
        $lids = [];
        $lArr = [];
        $list = $article->field('languageid')->where('flag_number', $flag_number)->select();
        if(!empty($list)){
            foreach ($list as $k=>$v) {
                $lids[] = $v['languageid'];
            }
            foreach ($languages as $k=>$v) {
                if(!in_array($v['languageid'],$lids)) {
                    $lArr[] = $v;
                }
            }
        } else {
            $lArr = $list;
        }
        $this->assign('languages',$lArr);
        $this->assign('flag_number',$flag_number);
        return $this->fetch('/article/create_translation');

    }
    public function delAction()
    {
        $articleid = input('param.articleid', false);
        $flag_number = input('param.flag_number', false);
        if($articleid){
            $res = ArticleM::destroy($articleid);
            if($res){
                if($flag_number) {
                    $this->success('del action success !', '/article/translation/flag_number/'.$flag_number);
                } else {
                    $this->success('del action success !', '/article/index');
                }
            }
        } else {
          $this->error('failed');
        }
    }
}
<?php

namespace app\admin\controller;


class UploadController extends ControllerInc
{
    public function oneImgAction()
    {
        $data = [];
        $file = request()->file('file');
        if($file){
            $info = $file->move(ROOT_PATH . 'public/uploads');
            if($info){

                $data['path'] = '/uploads/'.$info->getSaveName();
                $data['name'] = $info->getFilename();
                $data['code'] = 0;
            }else{
                $data['code'] = 106;
            }
        }
        return json($data);
    }

    public function kinEditUploadAction()
    {
        $data = [];
        $file = request()->file('imgFile');
        if($file){
            $info = $file->move(ROOT_PATH . 'public/uploads/kinEdit');
            if($info){

                $data['url'] = '/uploads/kinEdit/'.$info->getSaveName();
                $data['name'] = $info->getFilename();
                $data['error'] = 0;
            }else{
                $data['code'] = 106;
            }
        }
        return json($data);
    }
}
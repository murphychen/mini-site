<?php

namespace app\admin\controller;


use think\Controller;
use app\admin\model\Admin;
class AuthController extends Controller
{
    public function indexAction()
    {
        $this->view->engine->layout(false);
        if(request()->isPost()) {
            $input_key = input('post.key',false);
            $s_key = session('auth_key');
            $errors = '';
            if($input_key == $s_key) {
                $account = input('post.account',false);
                $psw = input('post.password',false);
                $row = Admin::where('account', $account)->find();
                if(empty($row)){
                    $errors = 'The account does not exist in the system.';
                    session('auth_err', $errors);
                    $this->redirect('/auth/index');
                }
                if($row['password'] != md5($row['salt'].$psw)) {
                    $errors = 'Wrong password';
                    session('auth_err', $errors);
                    $this->redirect('/auth/index');
                }
                $updateData = [];
                $updateData['login_count'] = $row['login_count'] + 1;
                $updateData['last_login_time'] = date("Y-m-d H:i:s");
                $updateData['login_ip'] =  request()->ip();
                $admin = new Admin();
                $admin->allowField(true)->save($updateData,['adminid' => $row['adminid']]);
                session(config('auth_session_key'),$row);
                session('auth_err', null);
                $this->redirect('/');
            } else {
                $errors = 'Blocking access';
                session('auth_err', $errors);
                $this->redirect('/auth/index');
            }
        }
        $key = md5(uniqid());
        session('auth_key', $key);
        $this->assign('key',$key);
        $out_err = session('auth_err') ? session('auth_err') : '';
        $this->assign('auth_err', $out_err);
        return $this->fetch('/auth/index');
    }

    public function logoutAction()
    {
        session(null);
        $this->redirect('/auth/index');
    }
}
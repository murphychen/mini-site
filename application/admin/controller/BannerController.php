<?php

namespace app\admin\controller;
use app\admin\model\Banner as BannerM;
use app\admin\validate\Banner;

class BannerController extends ControllerInc
{
    public function indexAction()
    {
        $banner = new BannerM();
        $list = $banner->order('create_time desc')->paginate();
        $page = $list->render();
        $list = $list->toArray();
        $this->assign('page', $page);
        $this->assign('list', $list['data']);
        return $this->fetch('/banner/index');
    }
    public function editAction()
    {
        $bannerid = input('param.bannerid',false);
        if(request()->isPost()){
            $banner = new BannerM();
            $postArr = input('post.');
            $bannerid = input('post.bannerid',false);
            if($bannerid){
                $result = $this->validate($postArr,'Banner.edit');
                if(false === $result){
                    $this->error($result);
                }
                $res = $banner->allowField(true)->save($postArr,['bannerid',$bannerid]);
                if($res) {
                    $this->success('edit action success !', '/banner/index');
                }else {
                    $this->error('edit action failed !');
                }
            } else {
                if(isset($postArr['file'])) {
                    unset($postArr['file']);
                }
                $result = $this->validate($postArr,'Banner.add');
                if(false === $result){
                    $this->error($result);
                }
                $res = $banner->allowField(true)->save($postArr);
                if($res) {
                    $this->success('add action success !', '/banner/index');
                }else {
                    $this->error('add action failed !');
                }
            }
        }
        if($bannerid) {
            $row = BannerM::get($bannerid);
            $this->assign('row', $row);
            return $this->fetch('/banner/edit');
        } else {
            return $this->fetch('/banner/add');
        }
    }

    public function delAction()
    {
        $bannerid = input('param.bannerid', false);
        if($bannerid){
            $res = BannerM::destroy($bannerid);
            if($res){
                $this->success('del action success !', '/banner/index');
            }
        } else {
            $this->error('failed');
        }
    }

}
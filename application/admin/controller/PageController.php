<?php
/**
 * Created by PhpStorm.
 * User: chenmf
 * Date: 2018/6/14
 * Time: 9:54
 */

namespace app\admin\controller;
use app\admin\model\Page as PageM;

class PageController extends ControllerInc
{
    public function indexAction()
    {
        $list = PageM::alias('p')
                ->field('p.*,s.sitename as siteName')
                ->join('__SITES__ s', 's.siteid=p.siteid','left')
                ->order('pageid asc')
                ->select();
        $this->assign('list', $list);

        return $this->fetch();
    }

    public function editAction()
    {
        $pageid = input('param.pageid',false);
        if(request()->isPost()){
            $page = new PageM();
            $postArr = input('post.');
            $pageid = input('post.pageid',false);
            if($pageid){
                $res = $page->allowField(true)->save($postArr,['pageid',$pageid]);
                if($res) {
                    $this->success('edit action success !', '/page/index');
                }else {
                    $this->error('edit action failed !');
                }
            } else {
                $res = $page->allowField(true)->save($postArr);
                if($res) {
                    $this->success('add action success !', '/page/index');
                }else {
                    $this->error('add action failed !');
                }
            }
        }
        if($pageid) {
            $row = PageM::get($pageid);
            $this->assign('row', $row);
            return $this->fetch('/page/edit');
        } else {
            return $this->fetch('/page/add');
        }
    }
}
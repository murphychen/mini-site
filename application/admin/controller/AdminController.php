<?php
namespace app\admin\controller;
use app\admin\model\Admin as AdminM;
use app\admin\validate\Admin;

class AdminController extends ControllerInc
{

    public function indexAction()
    {

        return $this->fetch('/admin/index');
    }

    public function getAjaxListAction()
    {
        $list = AdminM::paginate(50);
        $list = $list->toArray();
        $data = [];
        $data['code'] = 0;
        $data['msg'] = '';
        $data['count'] = $list['total'];
        $data['data'] = $list['data'];
        return json($data);
    }

    public function delAction()
    {
        $adminid = input('param.adminid',false);
        if($adminid){
            $res = AdminM::destroy($adminid);
            if($res) {
                return json(['code'=>0,'msg'=>'Action success!']);
            }
        }
        return json(['code'=>105,'msg'=>'Action failed!']);
    }

    public function editAction()
    {
        $admin = new AdminM();
        $adminid = input('param.adminid',false);
        if(request()->isPost()){
            $postArr = input('post.');
            $adminid = input('post.adminid',false);
            if($adminid) {
                if(!empty($postArr['password'])){
                    $row = AdminM::get($adminid);
                    $salt = $row['salt'];
                    $postArr['password'] = md5($salt.$postArr['password']);
                } else {
                    unset($postArr['password']);
                }
                $res = $admin->validate('Admin.edit')->allowField(true)->save($postArr, ['adminid'=>$adminid]);
            } else {
                $postArr['salt'] = uniqid();
                $postArr['password'] = md5($postArr['salt'].$postArr['password']);
                $res = $admin->validate('Admin.add')->allowField(true)->save($postArr);
            }
            if($res == false) {
                $this->error($admin->getError());
            } else {
                $this->success('Action success!','/admin/index');
            }
        };
        if ($adminid) {
            $row = AdminM::get($adminid);
            if(empty($row)){
                $this->error('Data does not exist');
            }
            $this->assign('row', $row);
            return $this->fetch('/admin/edit');
        } else {
            return $this->fetch('/admin/add');
        }
    }
}
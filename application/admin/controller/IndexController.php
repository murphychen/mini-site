<?php
namespace app\admin\controller;

class IndexController extends ControllerInc
{
    public function indexAction()
    {
        return $this->fetch('/dashboard/index');
    }
}
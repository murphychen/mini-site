<?php

namespace app\admin\controller;

use app\admin\model\GameTypes as GameTypesM;
use app\admin\validate\GameType;

class LottoController extends ControllerInc
{
    public function indexAction()
    {
        $getParams = input('param.');
        $getStr = '';
        if(!empty($getParams)) {
            foreach ($getParams as $k=>$v){
                if($v){
                    $getStr .= '/'.$k.'/'.$v;
                }
            }
        } else {
            $getParams = [];
        }
        $gameTpyes = new GameTypesM();
        $where = [];
        $gametypeid = input('param.gametypeid', false);
        $name = input('param.name', false);

        $gametypeid && $where['gametypeid'] = $gametypeid;
        $name && $where['name'] = ['like','%'. $name .'%'];
        if(!empty($where)) {
            $gameTpyes->where($where);
        }
        $list = $gameTpyes->order('isplayable desc,onhold asc,gametypeid asc')->paginate(100);
        $page = $list->render();
        $list = $list->toArray();
        $this->assign('list', $list['data']);
        $this->assign('page', $page);
        $this->assign('search', $getParams);
        $this->assign('getStr', $getStr);
        return $this->fetch('/lotto/index');
    }


    public function editAction()
    {
        $gametypeid = input('param.gametypeid',false);
        if(request()->isPost()){
            $gametype = new GameTypesM();
            $postArr = input('post.');
            $gametypeid = input('post.gametypeid',false);
            if($gametypeid){
                $result = $this->validate($postArr,'GameType.edit');
                if(false === $result){
                    $this->error($result);
                }
                if($postArr['on_banner'] == 1) {
                    $gametype->where('gametypeid','>',0)->update(['on_banner'=>0]);
                }
                $res = $gametype->allowField(true)->save($postArr,['gametypeid',$gametypeid]);
                if($res) {
                    $this->success('edit action success !', '/lotto/index');
                }else {
                    $this->error('edit action failed !');
                }
            } else {
                if(isset($postArr['file'])) {
                    unset($postArr['file']);
                }

                $res = $gametype->allowField(true)->save($postArr);
                if($res) {
                    $this->success('add action success !', '/lotto/index');
                }else {
                    $this->error('add action failed !');
                }
            }
        }
        if($gametypeid) {
            $row = GameTypesM::get($gametypeid);
            $this->assign('row', $row);
            return $this->fetch('/lotto/edit');
        } else {
            return $this->fetch('/lotto/add');
        }
    }

    public function delAction()
    {
        $gametypeid = input('param.gametypeid',false);
        if($gametypeid){
            $res = GameTypesM::destroy($gametypeid);
            if($res) {
                $this->success('del action success !', '/lotto/index');
            }
        }
        $this->error('del action failed !');
    }


}
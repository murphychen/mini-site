<?php


namespace app\admin\controller;
use app\admin\model\Games;
use app\admin\model\GamesPayouttables;
class ResultController extends ControllerInc
{
    public function indexAction()
    {
        $getParams = input('param.');
        $getStr = '';
        if(!empty($getParams)) {
            foreach ($getParams as $k=>$v){
                if(!empty($v)){
                    $getStr .= '/'.$k.'/'.$v;
                }
            }
        }
        $this->assign('search', $getParams);
        $this->assign('getStr', $getStr);
        return $this->fetch('/result/index');
    }


    public function ajaxGetResultDataAction()
    {
        $gametypeid = input('param.gametypeid', false);
        $name = input('param.name', false);
        $games = new Games();

        $gametypeid && $where['g.gametypeid'] = $gametypeid;
        $name && $where['gt.name'] = ['like', '%'. $name .'%'];

        if(!empty($where)) {
            $games->where($where);
        }
        $list = $games->alias('g')
                ->field('g.*,gt.name,gt.slug,gt.logo')
                ->join('__GAME_TYPES__ gt', 'gt.gametypeid = g.gametypeid')
                ->order('g.drawid desc')
                ->paginate();
        $list = $list->toArray();
        $data = [];
        $data['code'] = 0;
        $data['msg'] = 'Data does not exist';
        $data['count'] = $list['total'];
        $data['data'] = $list['data'];
        return json($data);
    }

    public function payoutAction()
    {
        $drawid = input('param.drawid',false);
        if($drawid > 0) {
            $this->assign('drawid', $drawid);
            return $this->fetch('/result/payout');
        } else {
            $this->error('Action success');
        }
    }

    public function ajaxGetPayoutDataAction()
    {
        $drawid = input('param.drawid',false);
        if($drawid > 0) {
            $this->assign('drawid', $drawid);
            $list = GamesPayouttables::where('drawid', $drawid)->order('sortorder asc')->paginate(100);
            $list = $list->toArray();
            $data = [];
            $data['code'] = 0;
            $data['msg'] = 'Data does not exist';
            $data['count'] = $list['total'];
            $data['data'] = $list['data'];
            return json($data);
        }
    }
}
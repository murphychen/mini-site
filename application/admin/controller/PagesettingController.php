<?php

namespace app\admin\controller;
use app\admin\model\PageSetting as PageSettingM;
use app\admin\validate\Pagesetting;
use app\admin\model\Languages;
use app\admin\model\GameTypes;


class PagesettingController extends ControllerInc
{
    public function indexAction()
    {
        $list = PageSettingM::alias('ps')
                ->field('ps.*,gt.name as gtName,lt.name  as lName')
                ->join('__GAME_TYPES__ gt','gt.gametypeid = ps.gametypeid', 'left')
                ->join('__LANGUAGES__ lt', 'lt.languageid = ps.languageid', 'left')
                ->order('ps.create_time desc')
                ->select();
        $this->assign('list', $list);
        return $this->fetch('/pagesetting/index');
    }

    public function editAction()
    {
        $pagesettingid = input('param.pagesettingid',false);
        if(request()->isPost()){
            $pagesetting = new PageSettingM();
            $postArr = input('post.');
            $pagesettingid = input('post.pagesettingid',false);
            if($pagesettingid){
                $result = $this->validate($postArr,'Pagesetting.edit');
                if(false === $result){
                    $this->error($result);
                }
                $res = $pagesetting->allowField(true)->save($postArr,['pagesettingid',$pagesettingid]);
                if($res) {
                    $this->success('edit action success !', '/pagesetting/index');
                }else {
                    $this->error('edit action failed !');
                }
            } else {
                $result = $this->validate($postArr,'Pagesetting.add');
                if(false === $result){
                    $this->error($result);
                }
                $res = $pagesetting->allowField(true)->save($postArr);
                if($res) {
                    $this->success('add action success !', '/pagesetting/index');
                }else {
                    $this->error('add action failed !');
                }
            }
        }
        $languages = (new Languages())->select();
        $this->assign('languages',$languages);
        $where = [];
        $where['isplayable'] = 1;
        $where['onhold'] = 0;
        $gameTypes = (new GameTypes())->where($where)->select();
        $this->assign('gameTypes',$gameTypes);
        if($pagesettingid) {
            $row = PageSettingM::get($pagesettingid);
            $this->assign('row', $row);
            return $this->fetch('/pagesetting/edit');
        } else {
            return $this->fetch('/pagesetting/add');
        }
    }
}
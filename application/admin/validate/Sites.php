<?php
namespace app\admin\validate;
use think\Validate;

class Sites extends Validate
{
    protected $rule =   [
        'sitename'  => 'require',
        'sitehost'   => 'require',
    ];

    protected $message  =   [
        'sitename.require' => 'Sitename required',
        'sitehost.require'     => 'Sitehost required',
    ];

    protected $scene = [
        'edit'  =>  ['sitename','sitehost'],
        'add'  =>  ['sitename','sitehost'],
    ];

}
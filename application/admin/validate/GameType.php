<?php
namespace app\admin\validate;
use think\Validate;

class GameType extends Validate
{
    protected $rule =   [
        'name'  => 'require',
        'slug'  => 'require',
        'logo'  => 'require',
        'bg_color'   => 'require',
        'currency'   => 'require',
        'currentjackpot'   => 'require',
        'country'   => 'require',
        'numbersdrawn'   => 'require',
        'numbermin'   => 'require',
        'numbermax'   => 'require',
        'priceperdraw'   => 'require',
        'purchaseprice'   => 'require',
        'websiteurl'   => 'require',
    ];

    protected $message  =   [
        'name.require' => 'name required',
        'slug.require' => 'slug required',
        'logo.require' => 'logo required',
        'currency.require' => 'currency required',
        'bg_color.require' => 'bg_color required',
        'currentjackpot.require'     => 'currentjackpot required',
        'country.require'     => 'country required',
        'numbersdrawn.require'     => 'numbersdrawn required',
        'numbermin.require'     => 'numbermin required',
        'numbermax.require'     => 'numbermax required',
        'priceperdraw.require'     => 'priceperdraw required',
        'purchaseprice.require'     => 'purchaseprice required',
        'websiteurl.require'     => 'websiteurl required',
    ];

    protected $scene = [
        'edit'  =>  ['name','slug','logo','currency','bg_color','currentjackpot','country','numbersdrawn','numbermin','numbermax','priceperdraw','websiteurl'],
        'add'  =>  ['name','slug','logo','currency','bg_color','currentjackpot','country','numbersdrawn','numbermin','numbermax','priceperdraw','websiteurl'],
    ];

}
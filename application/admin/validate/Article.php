<?php
namespace app\admin\validate;


use think\Validate;

class Article extends Validate
{
    protected $rule =   [
        'title'  => 'require',
        'cover'   => 'require',
        'content'   => 'require',
        'gametypeid'   => 'require',
    ];

    protected $message  =   [
        'title.require' => 'Title required',
        'cover.require'     => 'Cover required',
        'content.require'     => 'content required',
        'gametypeid.require'     => 'gametypeid required',
    ];

    protected $scene = [
        'edit'  =>  ['title','cover','content','gametypeid'],
        'add'  =>  ['title','cover','content','gametypeid'],
    ];
}
<?php
namespace app\admin\validate;
use think\Validate;

class Admin extends Validate
{
    protected $rule =   [
        'account'  => 'require',
        'password'   => 'require',
    ];

    protected $message  =   [
        'account.require' => 'Account required',
        'password.require'     => 'Password required',
    ];

    protected $scene = [
        'edit'  =>  ['account'],
        'add'  =>  ['account','password'],
    ];

}
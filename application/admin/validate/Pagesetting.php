<?php
namespace app\admin\validate;


use think\Validate;

class Pagesetting extends Validate
{
    protected $rule =   [
        'title'  => 'require',
        'pageid'   => 'require',
        'content'   => 'require',
        'languageid'   => 'require',
    ];

    protected $message  =   [
        'title.require' => 'Title required',
        'pageid.require'     => 'page id required',
        'content.require'     => 'content required',
        'languageid.require'     => 'languageid required',
    ];

    protected $scene = [
        'edit'  =>  ['title','pageid','content','languageid'],
        'add'  =>  ['title','pageid','content','languageid'],
    ];
}
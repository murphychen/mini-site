<?php
namespace app\admin\validate;

use think\Validate;

class Banner extends Validate
{
    protected $rule =   [
        'title'  => 'require',
        'src'   => 'require',
    ];

    protected $message  =   [
        'title.require' => 'Title required',
        'src.require'     => 'Src required',
    ];

    protected $scene = [
        'edit'  =>  ['title','src'],
        'add'  =>  ['title','src'],
    ];

}
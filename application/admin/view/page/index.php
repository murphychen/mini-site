<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b style="color: #5FB878">Page List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/page/edit" class="layui-btn">Add page</a>
    </div>
</div>
<table class="layui-table" lay-size="lg">
    <colgroup>
        <col width="50">
        <col>
        <col>
        <col>
        <col width="200">
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>pageName</th>
        <th>siteName</th>
        <th>status</th>
        <th>update time</th>
        <th>Action</th>
    </tr>
    </thead>
    {if condition="!empty($list)"}
    <tbody>
    {foreach name="list" item="vo"}
    <tr>
        <td>{$vo.pageid}</td>
        <td>{$vo.pagename}</td>
        <td>{$vo.siteName}</td>
        <td>
            {if condition="$vo.status == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>{$vo.update_time}</td>
        <td>
            <a class="layui-btn layui-btn-xs" href="/page/edit/pageid/{$vo.pageid}" lay-event="edit">Edit</a>
          <!--  <a class="layui-btn layui-btn-danger layui-btn-xs del-action" data-href="/page/del/pageid/{$vo.pageid}">Del</a>-->
        </td>
    </tr>
    {/foreach}
    </tbody>
    {/if}
</table>
<script>
    $(function () {
        layui.use('layer', function(){
            var layer = layui.layer;
            $(".del-action").click(function () {
                var href = $(this).data('href');
                layer.confirm('Are you sure you want to delete it?', {
                    btn: ['YES','CANCEL']
                }, function(index){
                    window.location.href = href;
                }, function(index){
                    layer.close(index);
                });
            })
        });
        layui.use('form', function(){
            var form = layui.form;
        });
    })
    $(".js-reset").click(function () {
        window.location.href = '/page/index';
    })
</script>
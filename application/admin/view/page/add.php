<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/page/index"><b>Page List</b></a>
            <a href="javascript:"><b style="color: #5FB878">Page Add</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">

    <div class="layui-form-item">
        <label class="layui-form-label">page name</label>
        <div class="layui-input-inline">
            <input type="text" name="pagename" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">show</label>
        <div class="layui-input-block">
            <input type="radio" name="status" value="1" title="YES" checked>
            <input type="radio" name="status" value="0" title="NO" >
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(formDemo)', function(data){
            $("#addForm").submit();
        });
    });
</script>
<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/article/index"><b>Article List</b></a>
            <a href="javascript:"><b style="color: #5FB878">Translation List</b></a>
        </span>
    </div>
    <div class="layui-card-body">
        <a href="/article/createTranslation/flag_number/{$flag_number}" class="layui-btn">create translation</a>
    </div>
</div>
<table class="layui-table" lay-size="lg">
    <colgroup>
        <col width="50">
        <col width="50">
        <col>
        <col>
        <col>
        <col>
        <col>
        <col width="60">
        <col width="60">
        <col width="60">
        <col width="150">
        <col width="200">
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>num</th>
        <th>Title</th>
        <th>Language</th>
        <th>Cover</th>
        <th>Intro</th>
        <th>gameTypeName</th>
        <th>sortOrder</th>
        <th>Status</th>
        <th>Hot</th>
        <th>update time</th>
        <th>Action</th>
    </tr>
    </thead>
    {if condition="!empty($list)"}
    <tbody>
    {foreach name="list" item="vo"}
    <tr>
        <td>{$vo.articleid}</td>
        <td width="20">{$vo.flag_number}</td>
        <td>{$vo.title}</td>
        <td>{$vo.lName}</td>
        <td><img src="{$vo.cover}" height="60" alt=""></td>
        <td>{$vo.intro}</td>
        <td>{$vo.gameTypeName}</td>
        <td>{$vo.sort_order}</td>
        <td>
            {if condition="$vo.status == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            {if condition="$vo.is_hot == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>{$vo.update_time}</td>
        <td>
            <a class="layui-btn layui-btn-xs" href="/article/edit/articleid/{$vo.articleid}/flag_number/{$vo.flag_number}" lay-event="edit">Edit</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs del-action" data-href="/article/del/articleid/{$vo.articleid}/flag_number/{$vo.flag_number}">Del</a>
        </td>
    </tr>
    {/foreach}
    </tbody>
    {/if}
</table>
<script>
    $(function () {
        layui.use('layer', function(){
            var layer = layui.layer;
            $(".del-action").click(function () {
                var href = $(this).data('href');
                layer.confirm('Are you sure you want to delete it?', {
                    btn: ['YES','CANCEL']
                }, function(index){
                    window.location.href = href;
                }, function(index){
                    layer.close(index);
                });
            })
        });
        layui.use('form', function(){
            var form = layui.form;
        });
    })
    $(".js-reset").click(function () {
        window.location.href = '/article/index';
    })
</script>
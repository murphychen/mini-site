<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b style="color: #5FB878">Article List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/article/edit" class="layui-btn">Add article</a>
    </div>
</div>

<form action="" class="layui-form" method="get">
<div class="layui-form-item">
    <div class="layui-inline">
        <label class="layui-form-label">Title</label>
        <div class="layui-input-inline">
            <input type="tel" name="title" value='{if condition="isset($search.title)"}{$search.title}{/if}' lay-verify="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">language</label>
        <div class="layui-input-inline">
            <select name="languageid" lay-search="">
                {foreach name="languages" item="vo"}
                <option value="{$vo.languageid}" {if condition="isset($search.languageid) && $search.languageid == $vo.languageid"}selected{/if}>{$vo.name}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="layui-inline">
        <button class="layui-btn" type="submit" lay-submit="" lay-filter="demo1">Search</button>
        <button type="reset" class="layui-btn layui-btn-primary js-reset">Reset</button>
    </div>
</div>
</form>
<table class="layui-table" lay-size="lg">
    <colgroup>
        <col width="50">
        <col width="50">
        <col>
        <col>
        <col>
        <col>
        <col>
        <col width="60">
        <col width="60">
        <col width="60">
        <col width="150">
        <col width="200">
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>num</th>
        <th>Title</th>
        <th>Language</th>
        <th>Cover</th>
        <th>Intro</th>
        <th>gameTypeName</th>
        <th>sortOrder</th>
        <th>Status</th>
        <th>Hot</th>
        <th>update time</th>
        <th>Action</th>
    </tr>
    </thead>
    {if condition="!empty($list)"}
    <tbody>
    {foreach name="list" item="vo"}
    <tr>
        <td>{$vo.articleid}</td>
        <td width="20">{$vo.flag_number}</td>
        <td>{$vo.title}</td>
        <td>{$vo.lName}</td>
        <td><img src="{$vo.cover}" height="60" alt=""></td>
        <td>{$vo.intro}</td>
        <td>{$vo.gametypes}</td>
        <td>{$vo.sort_order}</td>
        <td>
            {if condition="$vo.status == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            {if condition="$vo.is_hot == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>{$vo.update_time}</td>
        <td>
            <a class="layui-btn layui-btn-xs" href="/article/edit/articleid/{$vo.articleid}" lay-event="edit">Edit</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs del-action" data-href="/article/del/articleid/{$vo.articleid}">Del</a>
           <!-- <p style="margin-top: 5px;"><a class="layui-btn layui-btn-xs" href="/article/translation/flag_number/{$vo.flag_number}" lay-event="edit">Translation</a></p>-->
        </td>
    </tr>
    {/foreach}
    </tbody>
    {$page}
    {/if}
</table>
<script>
    $(function () {
        layui.use('layer', function(){
            var layer = layui.layer;
            $(".del-action").click(function () {
                var href = $(this).data('href');
                layer.confirm('Are you sure you want to delete it?', {
                    btn: ['YES','CANCEL']
                }, function(index){
                    window.location.href = href;
                }, function(index){
                    layer.close(index);
                });
            })
        });
        layui.use('form', function(){
            var form = layui.form;
        });
    })
    $(".js-reset").click(function () {
        window.location.href = '/article/index';
    })
</script>
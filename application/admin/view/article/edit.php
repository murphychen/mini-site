<link rel="stylesheet" href="/__PUBLIC__kindeditor/themes/default/default.css" />
<script charset="utf-8" src="/__PUBLIC__kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="/__PUBLIC__kindeditor/lang/en.js"></script>
<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/article/index"><b>Article List</b></a>
            <a href="javascript:"><b style="color: #5FB878">Article edit</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">
    <input type="hidden" name="articleid" value="{$row.articleid}">
    <div class="layui-form-item">
        <label class="layui-form-label">Title</label>
        <div class="layui-input-inline">
            <input type="text" name="title" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" value="{$row.title}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">sort order</label>
        <div class="layui-input-inline">
            <input type="text" name="sort_order" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input" value="{$row.sort_order}">
        </div>
    </div>
    <div class="layui-form-item" pane="">
        <label class="layui-form-label">game type</label>
        <div class="layui-input-block" id="js-game_types">
            {foreach name="gtlist" item="vo" key="k"}
                <input type="checkbox" name="game_type[{$k}]" lay-skin="primary" lay-filter="primaryChoose" title="{$vo.name}" {if condition="in_array($vo.gametypeid,$row.gametypeidArr)"} checked {/if} value="{$vo.gametypeid}|{$vo.name}">
            {/foreach}
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">cover</label>
        <div class="layui-input-inline">
            <button type="button" class="layui-btn" id="logoImg">
                <i class="layui-icon">&#xe67c;</i>Upload Image
            </button>
        </div>
        <img src="{$row.cover}" style="margin-left: 110px;" id="imgShow" width="100" alt="">
        <input type="hidden" name="cover" value="{$row.cover}" id="inputLogo">
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">show</label>
        <div class="layui-input-block">
            <input type="radio" name="status" value="1" title="YES" {if condition="$row.status == 1"} checked {/if}>
            <input type="radio" name="status" value="0" title="NO" {if condition="$row.status == 0"} checked {/if}>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">is hot</label>
        <div class="layui-input-block">
            <input type="radio" name="is_hot" value="1" title="YES" {if condition="$row.is_hot == 1"} checked {/if}>
            <input type="radio" name="is_hot" value="0" title="NO" {if condition="$row.is_hot == 0"} checked {/if}>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">intro</label>
        <div class="layui-input-inline">
            <textarea name="intro" placeholder="Please input remark" class="layui-textarea">{$row.intro}</textarea>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">content</label>
        <div class="layui-input-block">
            <textarea name="content" style="width:80%;height:600px;visibility:hidden;">{$row.content}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        form.on('submit(formDemo)', function(data){
            console.log(form);
            $("#addForm").submit();
        });
        //监听指定开关
        form.on('primary(primaryChoose)', function(data){
            console.log(22222)

        });

    });
    layui.use('upload', function(){
        var upload = layui.upload;
        var uploadInst = upload.render({
            elem: '#logoImg'
            ,url: '/upload/oneImg'
            ,exts: 'jpg|png|gif|bmp|jpeg'
            ,choose: function(obj){
                var files = obj.pushFile();
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
                if(res.code == 0) {
                    var path = res.path;
                    $("#imgShow").attr("src", path);
                    $("#inputLogo").val(path)
                }
            }
            ,error: function(){

            }
        });
    });

    KindEditor.ready(function(K) {
        var editor1 = K.create('textarea[name="content"]', {
            uploadJson : '/upload/kinEditUpload',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=example]')[0].submit();
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=example]')[0].submit();
                });
            }
        });
    });
    $(function () {
        $("#js-game_types input").on('change',function () {
            var title = $(this).attr('title');
            var id = $(this).val();
            console.log(title);
        })
    })

</script>
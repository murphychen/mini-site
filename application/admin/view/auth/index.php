<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>star site</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/__PUBLIC__layui/css/layui.css"  media="all">
    <link rel="stylesheet" href="/__PUBLIC__css/admin.css"  media="all">
    <script src="/__PUBLIC__layui/layui.js" charset="utf-8"></script>
    <script src="/__PUBLIC__js/jquery.js" charset="utf-8"></script>

</head>
<body id="auth-body">
<div class="layui-layout layui-layout-admin layui-layout-auth">
    <div id="auth-content" style="display: none">
        <form class="layui-form layui-form-pane layui-form-auth" action="" method="post">
            <input type="hidden" name="key" value="{$key}">
            <img src="/__PUBLIC__img/logo.png" alt="">
            <h2 style="color: green;margin-bottom: 20px;"> Sign me in </h2>
            <div class="layui-form-item">
                <label class="layui-form-label">Account</label>
                <div class="layui-input-inline">
                    <input type="text" name="account" lay-verify="required" placeholder="Please input account" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">Password</label>
                <div class="layui-input-inline">
                    <input type="password" name="password" placeholder="Please input password" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid layui-word-aux" style="color: red!important;">{$auth_err}</div>
            </div>

            <div class="layui-form-item">
                <button class="layui-btn" lay-submit="" type="submit" lay-filter="demo2">LOGIN</button>
            </div>
        </form>
    </div>

    <div class="layui-footer">
        © multilotto.com
    </div>
</div>
<script>
    layui.use(['form', 'layedit', 'laydate'], function(){

        var _html =  $("#auth-content").html();
        layer.open({
            type: 1,
            title: false,
            skin: 'layui-layer-rim',
            area: ['600px', '400px'],
            closeBtn: 0,
            content:_html,
            shade: 0
        });

    });

</script>


</body>
</html>
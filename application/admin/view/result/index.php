<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b style="color: #5FB878">Result List</b></a></span>
    </div>
</div>
<form action="" class="layui-form"">
<div class="layui-form-item">
    <div class="layui-inline">
        <label class="layui-form-label">Name</label>
        <div class="layui-input-inline">
            <input type="tel" name="name" value='{if condition="isset($search.name)"}{$search.name}{/if}' lay-verify="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">gameTypeId</label>
        <div class="layui-input-inline">
            <input type="text" name="gametypeid" lay-verify="" value='{if condition="isset($search.gametypeid)"}{$search.gametypeid}{/if}' autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-inline">
        <button class="layui-btn" lay-submit="" lay-filter="demo1">Search</button>
        <button type="reset" class="layui-btn layui-btn-primary js-reset">Reset</button>
    </div>
</div>
</form>
<table class="layui-table" lay-data="{height:600, url:'/result/ajaxGetResultData{$getStr}', page:true, id:'drawid'}" lay-filter="lotto">
    <thead>
    <tr>
        <th lay-data="{field:'drawid',width:40}">id</th>
        <th lay-data="{field:'name'}">name</th>
        <th lay-data="{field:'drawdate'}">draw date</th>
        <th lay-data="{field:'localdrawdatetime'}">local draw date time</th>
        <th lay-data="{field:'jackpotsize'}">jackpotsize</th>
        <th lay-data="{field:'jackpotcurrency'}">jackpot currency</th>
        <th lay-data="{field:'number1',templet: '#numberTpl'}">number</th>
        <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barSite'}">Action</th>
    </tr>
    </thead>
</table>
<script type="text/html" id="numberTpl">
   <p>{{d.number1}}-{{d.number2}}-{{d.number3}}-{{d.number4}}-{{d.number5}}
       <font color="red">{{d.number6 != null ? d.number6 : '*'}}-{{d.number7 != null ? d.number7 : '*'}}-{{d.number8 != null ? d.number8 : '*'}}</font>
   </p>
</script>
<script type="text/html" id="barSite">
    <a class="layui-btn layui-btn-xs" lay-event="payout">Payout</a>
</script>
<script>
    $(function () {
        layui.use('table', function(){
            var table = layui.table;
            table.on('tool(lotto)', function(obj){
                var data = obj.data;
                if(obj.event === 'payout'){
                    window.location.href = '/result/payout/drawid/'+data.drawid;
                }
            });
        });
        $(".js-reset").click(function () {
            window.location.href = '/result/index';
        })
    })
</script>
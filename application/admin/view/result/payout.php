<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/result/index"><b>Result List</b></a>
            /
             <a href="javascrpit:" ><b style="color: #5FB878">Result payout</b></a>
        </span>
    </div>
</div>
<table class="layui-table" lay-data="{height:600, url:'/result/ajaxGetPayoutData?drawid={$drawid}', page:true, id:'id'}" lay-filter="lotto">
    <thead>
    <tr>
        <!--<th lay-data="{field:'id',width:40}">id</th>-->
        <th lay-data="{field:'sortorder'}">Divisions</th>
        <th lay-data="{field:'numbers'}">Numbers</th>
        <th lay-data="{field:'extranumbers'}">Extra Numbers</th>
        <th lay-data="{field:'bonusnumbers'}">Bonus Numbers</th>
        <th lay-data="{field:'refundnumbers'}">Refund Numbers</th>
        <th lay-data="{field:'probability'}">Probability</th>
        <th lay-data="{field:'payout'}">Payout</th>
        <th lay-data="{field:'payoutcurrency'}">Payout Currency</th>
    </tr>
    </thead>
</table>

<script>
    $(function () {
        layui.use('table', function(){
            var table = layui.table;
            table.on('tool(lotto)', function(obj){
                var data = obj.data;
                if(obj.event === 'payout'){
                    window.location.href = '/result/payout/drawid/'+data.drawid;
                }
            });
        });
    })
</script>
<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/banner/index"><b>Banner List</b></a>
            <a href="javascript:"><b style="color: #5FB878">Banner Add</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">
    <div class="layui-form-item">
        <label class="layui-form-label">Title</label>
        <div class="layui-input-inline">
            <input type="text" name="title" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">Jump url</label>
        <div class="layui-input-inline">
            <input type="text" name="jump_url" required  placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">alt</label>
        <div class="layui-input-inline">
            <input type="text" name="alt" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">image</label>
        <div class="layui-input-inline">
            <button type="button" class="layui-btn" id="logoImg">
                <i class="layui-icon">&#xe67c;</i>Upload Image
            </button>
        </div>
        <img src="" style="margin-left: 110px;" id="imgShow" width="100" alt="">
        <input type="hidden" name="src" value="" id="inputLogo">
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">show</label>
        <div class="layui-input-block">
            <input type="radio" name="status" value="1" title="YES" checked>
            <input type="radio" name="status" value="0" title="NO" >
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">remark</label>
        <div class="layui-input-inline">
            <textarea name="remark" placeholder="Please input remark" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(formDemo)', function(data){
            $("#addForm").submit();
        });
    });
    layui.use('upload', function(){
        var upload = layui.upload;
        var uploadInst = upload.render({
            elem: '#logoImg'
            ,url: '/upload/oneImg'
            ,exts: 'jpg|png|gif|bmp|jpeg'
            ,choose: function(obj){
                var files = obj.pushFile();
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
                if(res.code == 0) {
                    var path = res.path;
                    $("#imgShow").attr("src", path);
                    $("#inputLogo").val(path)
                }
            }
            ,error: function(){

            }
        });
    });
</script>
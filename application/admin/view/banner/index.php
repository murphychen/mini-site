<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b style="color: #5FB878">Banner List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/banner/edit" class="layui-btn">Add Banner</a>
    </div>
</div>
<table class="layui-table" lay-size="lg">
    <colgroup>
        <col width="150">
        <col width="200">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Img</th>
        <th>Jump Url</th>
        <th>Alt</th>
        <th>Show</th>
        <th>Action</th>
    </tr>
    </thead>
    {if condition="!empty($list)"}
    <tbody>
    {foreach name="list" item="vo"}
    <tr>
        <td>{$vo.bannerid}</td>
        <td>{$vo.title}</td>
        <td><img src="{$vo.src}" height="80" alt=""></td>
        <td>{$vo.jump_url}</td>
        <td>{$vo.alt}</td>
        <td>
            {if condition="$vo.status == 1"}
             <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            <a class="layui-btn layui-btn-xs" href="/banner/edit/bannerid/{$vo.bannerid}" lay-event="edit">Edit</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" onclick="return confirm('Are you sure del it ?')" href="/banner/del/bannerid/{$vo.bannerid}" lay-event="edit">Del</a>
        </td>
    </tr>
    {/foreach}
    </tbody>
    {$page}
    {/if}
</table>
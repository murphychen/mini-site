<link rel="stylesheet" href="/__PUBLIC__kindeditor/themes/default/default.css" />
<script charset="utf-8" src="/__PUBLIC__kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="/__PUBLIC__kindeditor/lang/en.js"></script>
<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/pagesetting/index"><b>content List</b></a>
            <a href="javascript:"><b style="color: #5FB878">Article Add</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">
    <div class="layui-inline">
        <label class="layui-form-label">language</label>
        <div class="layui-input-inline">
            <select name="languageid" lay-verify="required" lay-search="">
                {foreach name="languages" item="vo"}
                <option value="{$vo.languageid}">{$vo.name}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <br>
    <br>
    <div class="layui-inline">
        <label class="layui-form-label">page</label>
        <div class="layui-input-inline">
            <select name="gametypeid" lay-verify="required" lay-search="">
                <option value="0">Home page</option>
                {foreach name="gameTypes" item="vo"}
                <option value="{$vo.gametypeid}">{$vo.name}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <br>
    <br>
    <div class="layui-form-item">
        <label class="layui-form-label">Title</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">show</label>
        <div class="layui-input-block">
            <input type="radio" name="status" value="1" title="YES" checked>
            <input type="radio" name="status" value="0" title="NO" >
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">content</label>
        <div class="layui-input-block">
            <textarea name="content" style="width:80%;height:600px;visibility:hidden;"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(formDemo)', function(data){
            $("#addForm").submit();
        });
    });
    layui.use('upload', function(){
        var upload = layui.upload;
        var uploadInst = upload.render({
            elem: '#logoImg'
            ,url: '/upload/oneImg'
            ,exts: 'jpg|png|gif|bmp|jpeg'
            ,choose: function(obj){
                var files = obj.pushFile();
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
                if(res.code == 0) {
                    var path = res.path;
                    $("#imgShow").attr("src", path);
                    $("#inputLogo").val(path)
                }
            }
            ,error: function(){

            }
        });
    });

    KindEditor.ready(function(K) {
        var editor1 = K.create('textarea[name="content"]', {
            uploadJson : '/upload/kinEditUpload',
            allowFileManager : true,
            afterCreate : function() {
                var self = this;
                K.ctrl(document, 13, function() {
                    self.sync();
                    K('form[name=example]')[0].submit();
                });
                K.ctrl(self.edit.doc, 13, function() {
                    self.sync();
                    K('form[name=example]')[0].submit();
                });
            }
        });
    });
</script>
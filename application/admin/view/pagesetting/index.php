<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b style="color: #5FB878">pageSetting List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/pagesetting/edit" class="layui-btn">Add pageSetting</a>
    </div>
</div>
<table class="layui-table" lay-size="lg">
    <colgroup>
        <col width="50">
        <col width="50">
        <col>
        <col>
        <col>
        <col>
        <col>
        <col width="60">
        <col width="60">
        <col width="60">
        <col width="150">
        <col width="200">
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Page</th>
        <th>Language</th>
        <th>Status</th>
        <th>update time</th>
        <th>Action</th>
    </tr>
    </thead>
    {if condition="!empty($list)"}
    <tbody>
    {foreach name="list" item="vo"}
    <tr>
        <td>{$vo.pagesettingid}</td>
        <td>{$vo.title}</td>
        <td>
            {if condition="$vo.gametypeid == 0"}
            Home Page
            {else /}
            {$vo.gtName}
            {/if}

        </td>
        <td>{$vo.lName}</td>
        <td>
            {if condition="$vo.status == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>{$vo.update_time}</td>
        <td>
            <a class="layui-btn layui-btn-xs" href="/pagesetting/edit/pagesettingid/{$vo.pagesettingid}" lay-event="edit">Edit</a>
        </td>
    </tr>
    {/foreach}
    </tbody>
    {/if}
</table>
<script>
    $(function () {
        layui.use('layer', function(){
            var layer = layui.layer;
            $(".del-action").click(function () {
                var href = $(this).data('href');
                layer.confirm('Are you sure you want to delete it?', {
                    btn: ['YES','CANCEL']
                }, function(index){
                    window.location.href = href;
                }, function(index){
                    layer.close(index);
                });
            })
        });
        layui.use('form', function(){
            var form = layui.form;
        });
    })
    $(".js-reset").click(function () {
        window.location.href = '/pageSetting/index';
    })
</script>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>star site</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/__PUBLIC__layui/css/layui.css"  media="all">
    <link rel="stylesheet" href="/__PUBLIC__css/admin.css"  media="all">
    <script src="/__PUBLIC__js/jquery.js"></script>
    <script src="/__PUBLIC__layui/layui.js" charset="utf-8"></script>
    <script src="/__PUBLIC__js/admin.js"></script>

</head>
<body>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">MM TEAM</div>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:" style="font-weight: bolder;color: #009688">Current Site: {if condition="isset($current_sites.sitename)"}{$current_sites.sitename}{/if}</a>
            </li>
            <li class="layui-nav-item" lay-unselect="">
                <a href="javascript:;">Site Change</a>
                <dl class="layui-nav-child">
                    {if condition="!empty($sites)"}
                    {foreach name="sites" item="vo"}
                    <dd><a href="javascript:;">{$vo.sitename}</a></dd>
                    {/foreach}
                    {/if}
                </dl>
            </li>
            <li class="layui-nav-item" lay-unselect="">
                <a href="javascript:;"><img src="http://t.cn/RCzsdCq" class="layui-nav-img">{$adminInfo['account']}</a>
                <dl class="layui-nav-child">
                    <dd><a href="/auth/logout">logout</a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="demo" style="margin-right: 10px;height: 100%">
                {if condition="$controller == 'index' || $controller == ''"}
                    <li class="layui-nav-item layui-this">
                {else}
                    <li class="layui-nav-item">
                {/if}
                    <a href="/">dashboard</a>
                </li>
                {if condition="$controller == 'lotto' || $controller == 'result'"}
                    <li class='layui-nav-item  layui-nav-itemed'>
                {else}
                    <li class='layui-nav-item'>
                {/if}
                    <a href="javascript:;">Lotto</a>
                    <dl class="layui-nav-child">
                        <dd {if condition="$controller == 'lotto'"} class="layui-this" {/if}>
                            <a href="/lotto/index">List</a>
                        </dd>
                        <dd {if condition="$controller == 'result'"} class="layui-this" {/if}><a href="/result/index">Lotto result</a></dd>
                    </dl>
                </li>
                {if condition="$controller == 'pagesetting'"}
                <li class="layui-nav-item layui-nav-itemed layui-this">
                    {else}
                <li class="layui-nav-item">
                    {/if}

                    <a href="/pagesetting/index">content</a></li>

                <li class="layui-nav-item">
                    <a href="javascript:;">Translation</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">Translation Strings</a></dd>
                        <dd><a href="javascript:;">String create</a></dd>
                    </dl>
                </li>
                {if condition="$controller == 'banner'"}
                    <li class="layui-nav-item layui-nav-itemed layui-this">
                {else}
                    <li class="layui-nav-item">
                {/if}
                    <a href="/banner/index">Banner</a>
                </li>
                {if condition="$controller == 'article'"}
                    <li class="layui-nav-item layui-nav-itemed layui-this">
                {else}
                    <li class="layui-nav-item">
                {/if}

                    <a href="/article/index">Article</a></li>

                {if condition="$controller == 'site'"}
                    <li class="layui-nav-item layui-nav-itemed layui-this">
                {else}
                    <li class="layui-nav-item">
                {/if}

                    <a href="/site/index">Site management</a>
                </li>
                {if condition="$controller == 'admin'"}
                    <li class="layui-nav-item layui-nav-itemed layui-this">
                {else}
                    <li class="layui-nav-item">
                {/if}

                    <a href="/admin/index">Admin management</a></li>
            </ul>
        </div>
    </div>
    <div class="layui-body">
        <div style="padding: 15px;">{__CONTENT__}</div>
    </div>
    <div class="layui-footer">
        © multilotto.com
    </div>
</div>
<script>
    layui.use('element', function(){
        var element = layui.element;
        element.on('nav(demo)', function(elem){

        });
    });
</script>

</body>
</html>
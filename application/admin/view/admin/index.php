<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b>Admin List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/admin/edit" class="layui-btn">Add admin user</a>
    </div>
</div>
<table class="layui-table" lay-data="{height:500, url:'/admin/getAjaxList', page:true, id:'adminid'}" lay-filter="admin">
    <thead>
    <tr>
        <th lay-data="{field:'adminid', width:80}">Admin id</th>
        <th lay-data="{field:'account'}">Account</th>
        <th lay-data="{field:'siteid'}">Site id</th>
        <th lay-data="{field:'phone'}">Phone</th>
        <th lay-data="{field:'email'}">Email</th>
        <th lay-data="{field:'login_count'}">Login count</th>
        <th lay-data="{field:'login_ip'}">Last login ip</th>
        <th lay-data="{field:'last_login_time'}">Last login time</th>
        <th lay-data="{field:'status',templet: '#statusTpl'}">Status</th>
        <th lay-data="{field:'remark'}">Remark</th>
        <th lay-data="{field:'update_time'}">Update time</th>
        <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barSite'}"></th>
    </tr>
    </thead>
</table>
<script type="text/html" id="statusTpl">
    {{#  if(d.status == 1){ }}
    <b style="color: green;">ON</b>
    {{#  } else { }}
    <b style="color: red;">OFF</b>
    {{#  } }}
</script>
<script type="text/html" id="barSite">
    <a class="layui-btn layui-btn-xs" lay-event="edit">Edit</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">Del</a>
</script>
<script>
    $(function () {
        layui.use('table', function(){
            var table = layui.table;
            table.on('tool(admin)', function(obj){
                var data = obj.data;
                if(obj.event === 'del'){
                    layer.confirm('Real delete it ?', function(index){
                        $.myHttp.del('/admin/del',{adminid:data.adminid})
                            .done(function (res) {
                                if(res.code == 0) {
                                    obj.del();
                                    layer.close(index);
                                }
                                layer.msg(res.msg, {time: 3000, icon:6});
                            })
                    });
                } else if(obj.event === 'edit'){
                    window.location.href = '/admin/edit/adminid/'+data.adminid;
                }
            });
        });
    })
</script>
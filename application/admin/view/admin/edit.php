<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/admin/index"><b>Admin List</b></a>
            <a href="javascript:"><b>Admin Edit</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">
    <input type="hidden" name="adminid" value="{$row.adminid}">
    <div class="layui-form-item">
        <label class="layui-form-label">account</label>
        <div class="layui-input-block">
            <input type="text" name="account" required  lay-verify="required" placeholder="Please input account" autocomplete="off" class="layui-input" value="{$row.account}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">password</label>
        <div class="layui-input-block">
            <input type="text" name="password" required  placeholder="If not, no modification." autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">phone</label>
        <div class="layui-input-block">
            <input type="text" name="phone" required  lay-verify="required" placeholder="Please input phone" autocomplete="off" class="layui-input" value="{$row.phone}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">email</label>
        <div class="layui-input-block">
            <input type="text" name="email" required  lay-verify="required" placeholder="Please input email" autocomplete="off" class="layui-input" value="{$row.email}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">status</label>
        <div class="layui-input-block">
            <input type="radio" {if condition="$row.status == 1"} checked {/if} name="status" value="1" title="on" >
            <input type="radio" name="status" value="2" {if condition="$row.status == 2"} checked {/if} title="off" >
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">remark</label>
        <div class="layui-input-block">
            <textarea name="remark" placeholder="Please input remark" class="layui-textarea">{$row.remark}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(formDemo)', function(data){
            $("#addForm").submit();
        });
    });
</script>
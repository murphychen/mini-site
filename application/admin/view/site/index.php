<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b>Site List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/site/edit" class="layui-btn">Add Site</a>
    </div>
</div>
<table class="layui-table" lay-data="{height:500, url:'/site/getAjaxList', page:true, id:'siteid'}" lay-filter="site">
    <thead>
    <tr>
        <th lay-data="{field:'siteid', width:80, sort: true, fixed: true}">Site id</th>
        <th lay-data="{field:'sitename'}">Site name</th>
        <th lay-data="{field:'sitehost'}">Site host</th>
        <th lay-data="{field:'status',templet: '#statusTpl'}">Status</th>
        <th lay-data="{field:'remark'}">Remark</th>
        <th lay-data="{field:'update_time'}">Update time</th>
        <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barSite'}"></th>
    </tr>
    </thead>
</table>
<script type="text/html" id="statusTpl">
    {{#  if(d.status == 1){ }}
    <b style="color: green;">ON</b>
    {{#  } else { }}
    <b style="color: red;">OFF</b>
    {{#  } }}
</script>
<script type="text/html" id="barSite">
    <a class="layui-btn layui-btn-xs" lay-event="edit">Edit</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">Del</a>
</script>
<script>
    $(function () {
        layui.use('table', function(){
            var table = layui.table;
            table.on('tool(site)', function(obj){
                var data = obj.data;
                if(obj.event === 'del'){
                    layer.confirm('Real delete it ?', function(index){
                        $.myHttp.del('/site/del',{siteid:data.siteid})
                            .done(function (res) {
                                if(res.code == 0) {
                                    obj.del();
                                    layer.close(index);
                                }
                                layer.msg(res.msg, {time: 3000, icon:6});
                            })
                    });
                } else if(obj.event === 'edit'){
                    window.location.href = '/site/edit/siteid/'+data.siteid;
                }
            });
        });
    })
</script>
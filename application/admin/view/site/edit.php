<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/site/index"><b>Site List</b></a>
            <a href="javascript:"><b>Site Edit</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">
    <input type="hidden" name="siteid" value="{$row.siteid}">
    <div class="layui-form-item">
        <label class="layui-form-label">site name</label>
        <div class="layui-input-block">
            <input type="text" name="sitename" required  lay-verify="required" placeholder="Please input site name" autocomplete="off" class="layui-input" value="{$row.sitename}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">site host</label>
        <div class="layui-input-block">
            <input type="text" name="sitehost" required  lay-verify="required" placeholder="Please input site host" autocomplete="off" class="layui-input" value="{$row.sitehost}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">status</label>
        <div class="layui-input-block">
            <input type="radio" {if condition="$row.status == 1"} checked {/if} name="status" value="1" title="on" >
            <input type="radio" name="status" value="2" {if condition="$row.status == 2"} checked {/if} title="off" >
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">remark</label>
        <div class="layui-input-block">
            <textarea name="remark" placeholder="Please input remark" class="layui-textarea">{$row.remark}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(formDemo)', function(data){
            $("#addForm").submit();
            //layer.msg(JSON.stringify(data.field));
            //return false;
        });
    });
</script>
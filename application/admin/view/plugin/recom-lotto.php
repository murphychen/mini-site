<!-- 文章中间的彩种推荐 -->
<div class="article-recom-lotto clearfix">
    <div class="count-time-cont">
        <div class="ctc-tit">New Draw</div>
        <div class="ctc-list">
            <ul class="ctc-list-box clearfix">
                <li class="ctc-item">
                    <span class="ctc-num-box">0</span>
                    <span class="ctc-num-box">2</span>
                    <div class="ctc-time">Days</div>
                </li>
                <li class="ctc-item">
                    <span class="ctc-num-box">1</span>
                    <span class="ctc-num-box">5</span>
                    <div class="ctc-time">Hours</div>
                </li>
                <li class="ctc-item">
                    <span class="ctc-num-box">4</span>
                    <span class="ctc-num-box">6</span>
                    <div class="ctc-time">Minutes</div>
                </li>
                <li class="ctc-item">
                    <span class="ctc-num-box ctc-num-red">5</span>
                    <span class="ctc-num-box ctc-num-red">9</span>
                    <div class="ctc-time">SecondS</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="recom-next">
        <div class="recom-next-jac">Next Jackpot</div>
        <div class="recom-jac-num">€45 Miliion</div>
        <div class="button-bt1">Play Now</div>
    </div>
</div>
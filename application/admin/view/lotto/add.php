<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb">
            <a href="/lotto/index"><b>Lotto List</b></a>
            <a href="javascript:"><b>Lotto Add</b></a>
        </span>
    </div>
</div>
<form class="layui-form" method="post" id="addForm">
    <div class="layui-form-item">
        <label class="layui-form-label">name</label>
        <div class="layui-input-inline">
            <input type="text" name="name" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">slug</label>
        <div class="layui-input-inline">
            <input type="text" name="slug" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">logo</label>
        <div class="layui-input-inline">
            <button type="button" class="layui-btn" id="logoImg">
                <i class="layui-icon">&#xe67c;</i>Upload Image
            </button>
        </div>
        <img src="" style="margin-left: 110px;" id="imgShow" width="100" alt="">
        <input type="hidden" name="logo" value="" id="inputLogo">
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">bg color class name</label>
        <div class="layui-input-inline">
            <input type="text" name="bg_color" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b>australiapowerball</b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">currency</label>
        <div class="layui-input-inline">
            <input type="text" name="currency" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">Please input currency</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">current jackpot</label>
        <div class="layui-input-inline">
            <input type="text" name="currentjackpot" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">next draw</label>
        <div class="layui-input-inline">
            <input type="text" name="nextdraw" class="layui-input" id="nextdraw" lay-verify="required" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b>2018-05-24 02:02:02</b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">cut off hours</label>
        <div class="layui-input-inline">
            <input type="text" name="cutoffhours" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">country</label>
        <div class="layui-input-inline">
            <input type="text" name="country" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b>GB/IE</b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">numbers drawn</label>
        <div class="layui-input-inline">
            <input type="text" name="numbersdrawn" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">extra numbers drawn</label>
        <div class="layui-input-inline">
            <input type="text" name="extranumbersdrawn" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">number min</label>
        <div class="layui-input-inline">
            <input type="text" name="numbermin" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">number max</label>
        <div class="layui-input-inline">
            <input type="text" name="numbermax" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">price per draw</label>
        <div class="layui-input-inline">
            <input type="text" name="priceperdraw" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">purchase price</label>
        <div class="layui-input-inline">
            <input type="text" name="purchaseprice" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">website url</label>
        <div class="layui-input-inline">
            <input type="text" name="websiteurl" required  lay-verify="required" placeholder="" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">For example: <b></b></div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">isplayable</label>
        <div class="layui-input-block">
            <input type="radio" name="isplayable" value="1" title="YES" checked>
            <input type="radio" name="isplayable" value="0" title="NO" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">onhold</label>
        <div class="layui-input-block">
            <input type="radio" name="onhold" value="1" title="YES" checked>
            <input type="radio" name="onhold" value="0" title="NO" >
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">remark</label>
        <div class="layui-input-inline">
            <textarea name="remark" placeholder="Please input remark" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">settings</label>
        <div class="layui-input-inline">
            <textarea name="settings" placeholder="" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">Submit</button>
            <button type="reset" class="layui-btn layui-btn-primary">Reset</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(formDemo)', function(data){
            $("#addForm").submit();
        });
    });
    layui.use('upload', function(){
        var upload = layui.upload;
        var uploadInst = upload.render({
            elem: '#logoImg'
            ,url: '/upload/oneImg'
            ,exts: 'jpg|png|gif|bmp|jpeg'
            ,choose: function(obj){
                var files = obj.pushFile();
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
                if(res.code == 0) {
                    var path = res.path;
                    $("#imgShow").attr("src", path);
                    $("#inputLogo").val(path)
                }
            }
            ,error: function(){

            }
        });
    });

    layui.use('laydate', function(){
        var laydate = layui.laydate;
        laydate.render({
            elem: '#nextdraw'
            ,type: 'datetime'
        });

    })
</script>
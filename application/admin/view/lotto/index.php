<div class="layui-card">
    <div class="layui-card-header">
        <span class="layui-breadcrumb"><a href="javascript:"><b style="color: #5FB878">Lotto List</b></a></span>
    </div>
    <div class="layui-card-body">
        <a href="/lotto/edit" class="layui-btn">Add Lotto</a>
    </div>
</div>
<form action="" class="layui-form"">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">Name</label>
            <div class="layui-input-inline">
                <input type="tel" name="name" value='{if condition="isset($search.name)"}{$search.name}{/if}' lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-inline">
                <input type="text" name="gametypeid" lay-verify="" value='{if condition="isset($search.gametypeid)"}{$search.gametypeid}{/if}' autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <button class="layui-btn" type="submit" lay-submit="" lay-filter="demo1">Search</button>
            <button type="reset" class="layui-btn layui-btn-primary js-reset">Reset</button>
        </div>
    </div>
</form>
<table class="layui-table">
    <colgroup>
        <col width="50">
        <col width="100">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th>ID</th>
        <th>name</th>
        <th>logo</th>
        <th>currency</th>
        <th>jackpot</th>
        <th>nextdraw</th>
        <th>cutoffhours</th>
        <th>country</th>
        <th>numbersdrawn</th>
        <th>playable</th>
        <th>onhold</th>
        <th>on banner</th>
        <th>on home_page</th>
        <th>on nav</th>
        <th>action</th>
    </tr>
    </thead>
    {if condition="!empty($list)"}
    <tbody>
    {foreach name="list" item="vo"}
    <tr>
        <td>{$vo.gametypeid}</td>
        <td>{$vo.name}</td>
        <td><img src="{$vo.logo}" height="40" alt=""></td>
        <td>{$vo.currency}</td>
        <td>{$vo.currentjackpot}</td>
        <td>{$vo.nextdraw}</td>
        <td>{$vo.cutoffhours}</td>
        <td>{$vo.country}</td>
        <td>{$vo.numbersdrawn}</td>
        <td>
            {if condition="$vo.isplayable == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            {if condition="$vo.onhold == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            {if condition="$vo.on_banner == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            {if condition="$vo.on_home_page == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            {if condition="$vo.on_nav == 1"}
            <b style="color: blue">YES</b>
            {else}
            <b style="color: grey">NO</b>
            {/if}
        </td>
        <td>
            <a class="layui-btn layui-btn-xs" href="/lotto/edit/gametypeid/{$vo.gametypeid}">Edit</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" onclick="return confirm('Are you sure del it ?')" href="/lotto/del/gametypeid/{$vo.gametypeid}">Del</a>
            <p style="margin-top: 5px;"><a class="layui-btn layui-btn-xs" href="/result/index/gametypeid/{$vo.gametypeid}">Result List</a></p>
        </td>
    </tr>
    {/foreach}
    </tbody>
    {$page}
    {/if}
</table>

<script>
    $(function () {
        layui.use('table', function(){
            var table = layui.table;
        });
        $(".js-reset").click(function () {
            window.location.href = '/lotto/index';
        })
    })
</script>
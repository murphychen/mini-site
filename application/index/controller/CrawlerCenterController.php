<?php

namespace app\index\controller;

use app\index\model\CrawlerCenter;
use think\Controller;
use think\Request;

class CrawlerCenterController extends Controller
{
    public function updateGamesAction() {
        $param = Request::instance()->param();
        $param = $param['data'] ?? null;
        if (empty($param)) {
            echo json_encode(['code' => CrawlerCenter::EMPTY_DATA, 'msg' => 'data empty !']);exit();
        }
        $param = json_decode($param,true);

        $dataType = isset($param['dataType']) ? $param['dataType'] : '';
        $list = isset($param['data']) ? $param['data'] : '';
        $inputSign = isset($param['sign']) ? $param['sign'] : null;
        $sign = md5($dataType.CrawlerCenter::signkey);

        if ($sign !== $inputSign) {
            echo json_encode(['code' => CrawlerCenter::SIGN_INVALID, 'msg' => 'sign error !']);exit();
        }
        switch ($dataType) {
            case 'draws':
                $res = CrawlerCenter::insertDraws($list);
                break;
            case 'result':
                $res = CrawlerCenter::insertPayout($list);
                break;
            default:
                echo json_encode(['code' => CrawlerCenter::INVALID_DATA_TYPE, 'msg' => 'datatype error !']);exit();
        }
        echo json_encode(['code' => CrawlerCenter::SUCCESS, 'msg' => 'run success full', 'data' => $res]);exit();
    }
}
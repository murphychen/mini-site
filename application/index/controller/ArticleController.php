<?php
namespace app\index\controller;
use think\Db;
use think\Cache;

class ArticleController extends ControllerInc
{
    public function indexAction()
    {

        $last_articles = Cache::get('last_articles_'.$this->lang);
        if ($last_articles === false) {
            $last_articles = Db::name('article')
                ->alias('a')
                ->field('a.cover,a.articleid,a.title,a.status,a.update_time,a.sort_order,a.gametypes,a.gametypeids,a.intro')
                ->where('languageid',$this->langid)
                ->where('status',1)->order('update_time desc')->limit(10)->select();
            if(!empty($last_articles)){
                foreach ($last_articles as $k=>$v){
                    if(!empty($v['gametypes'])) {
                        $last_articles[$k]['gameTypes'] = explode("|", $v['gametypes']);
                    }
                    $last_articles[$k]['slug'] = str_replace(" ", "-", $v['title']);
                }
            }
            Cache::set('last_articles_'.$this->lang, $last_articles,10);
        }
        $this->assign('last_articles',$last_articles);
        $this->recommendGameTypes();
        return $this->fetch();
    }

    public function detailAction()
    {
        $slug = input('param.slug',false);
        $detail = Cache::get('article_'.$slug);
        if($detail === false && $slug){
            $where = [];
            $where['title'] = str_replace("-", " ", $slug);;
            $detail = Db::name('article')->where($where)->find();
            if(!empty($detail['gametypes'])) {
                $detail['gameTypes'] = explode("|", $detail['gametypes']);
            }else{
                $this->error('404');
            }
            $detail['slug'] = str_replace(" ", "-", $detail['title']);
            Cache::set('article_'.$slug, $detail,60);
        }
        $this->assign('detail',$detail);


        $this->recommendArticles();
        $this->recommendGameTypes();
        $this->title = $detail['title'];
        $this->setHtmlTitle();
        return $this->fetch();
    }
}
<?php

namespace app\index\controller;


use think\Controller;

class EmptyController extends ControllerInc
{
    public function emptyAction(){

        $this->error($msg = '404', $url = '/', '', $wait = 3, $header = ['code'=>404,'Status'=>404]);
    }
}
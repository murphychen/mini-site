<?php
/**
 * Created by PhpStorm.
 * User: chenmf
 * Date: 2018/7/6
 * Time: 18:55
 */

namespace app\index\controller;
use think\Lang;

class LangController extends ControllerInc
{
    public function __construct()
    {
        parent::__construct();
    }

    public function changeLocaleAction()
    {
        $langs = ['en','de','sv','fi','no','es'];
        if(request()->isAjax()) {
            $locale = input('post.locale',false);
            $pathname = input('post.pathname',false);
            $locale = strtolower($locale);
            if($pathname != '' || $pathname != '/'){
                $pathname = substr($pathname,1);
                $arr = explode("/",$pathname);
                if($locale == 'en'){
                    $locale_str = '/';
                }else{
                    $locale_str = '/'.$locale.'/';
                }
                if(!in_array($arr[0],$langs)){
                    $url = $locale_str.$pathname;
                }else{
                    $pathname = substr($pathname,3);
                    $url = $locale_str.$pathname;
                }
            }else{
                $url = $locale == 'en'?'':'/'.$locale;
            }

            if($locale && in_array($locale,$langs)){
                Lang::range($locale);
                request()->langset(Lang::range());
                Lang::load([
                    THINK_PATH . 'lang' . DS . request()->langset() . EXT,
                    APP_PATH . 'lang' . DS . request()->langset() . EXT,
                ]);
                session('think_var',$locale);
                return json(['code'=>200,'url'=>$url]);
            }
        }
    }
}
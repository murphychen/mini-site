<?php


namespace app\index\controller;


use think\Controller;
use think\Db;
use think\Cache;
use think\Lang;
use think\Request;
use think\Exception;
class ControllerInc extends Controller
{
    public $action = '';
    public $controller = '';
    public $title = '';
    public $description = '';
    public $keyworld = '';
    public $lang = 'en';
    public $langid = 1;
    public $params_str = '';
    public function __construct()
    {
        Lang::setAllowLangList(['en','de','sv','fi','no','es']);
        parent::__construct();
        $lang = input('param.lang',false);
        if(!request()->isAjax()){
            if($lang){
                $this->lang = $lang;
            }else{
                $this->lang = 'en';
            }
            if($this->lang != Lang::range()){}
            Lang::range($this->lang);
            request()->langset(Lang::range());
            Lang::load([
                THINK_PATH . 'lang' . DS . request()->langset() . EXT,
                APP_PATH . 'lang' . DS . request()->langset() . EXT,
            ]);
            session('think_var',$this->lang);

        }
        $params = input('param.');
        $params_str = '/';
        if(!empty($params) && !request()->isAjax() ) {
            foreach ($params as $k=>$v){
                if($k != 'lang') {
                    if($params_str == '/'){
                        $params_str .= $v;
                    }else{
                        $params_str .= '/'.$v;
                    }
                }
            }
        }
        $len = strlen($params_str);
        if($len > 0 && $params_str[$len-1] == '/'){
            $params_str = substr($params_str,0,-1);
        }
        $this->getMoreGames();
        $this->getLanguage();
        $this->getAC();
        $this->setHtmlTitle();
        $logo_class = $this->lang == 'sv'?'se':$this->lang;
        $this->assign('logo_class',$logo_class);
        $base_link = config('base_link');
        if($this->lang == 'en') {
            $this->assign('lang','');
        } else {
            $this->assign('lang','/'.$this->lang);
        }
        $this->assign('langHead',$this->lang);
        $this->assign('params_str',$params_str);
        $userContry = $this->getUserContry();
//         $disablePlayContry = ['US','SG','TW','HK','IN'];//美国、新加坡、台湾、香港、印度
//         $ablePlayContry = ['MX'];//墨西哥
        $this->assign('isPlayContry',true);
//         if(in_array($userContry,$ablePlayContry)){
//             $this->assign('isPlayContry',true);
//         }else{
//             $this->assign('isPlayContry',false);
//         }
    }

    public function setHtmlTitle($game='',$date='',$numbers='')
    {
        if($this->controller == 'index'){
            $this->title = Lang::get('home_page_title');
            //print_r($this->title);exit;
            $this->description = Lang::get('home_page_description');
        }
        if($this->controller == 'article') {
            if($this->action == 'index') {
                $this->title = Lang::get('articles_page_title');
            }
        }
        if($this->controller == 'result'){
            if($this->action == 'gamedetail'){
                $this->title = Lang::get('gametype_page_title',[$game]);
                $this->description = Lang::get('gametype_page_description',[$game,$game]);
            }elseif ($this->action == 'index') {
                $this->title = Lang::get('result_page_title',[$game,$date]);
                $this->description = Lang::get('result_page_description',[$game,$date,$game,$numbers]);
            }
        }
        $this->assign('title', $this->title);
        $this->assign('description', $this->description);
    }
    private function getAC()
    {
        $action = request()->action();
        $controller = request()->controller();
        $this->action = strtolower($action);
        $this->controller = strtolower($controller);
        $this->assign('action',strtolower($action));
        $this->assign('controller',strtolower($controller));
    }
    private function getLanguage()
    {
        $ls = Cache::get('languages_all');
        if($ls === false) {
            $res = Db::name('languages')->select();
            $ls = [];
            foreach ($res as $k=>$v){
                $locale = strtolower($v['locale']);
                $ls[$locale] = $v;
            }
            Cache::set('languages_all',$ls,3600);
        }
        $this->langid = $ls[$this->lang]['languageid'];

        $this->assign('languages',$ls);
    }
    public function getMoreGames()
    {
        $where = [];
        $where['isplayable'] = 1;
        //$where['onhold'] = 0;
        $moreGame = Cache::get('game_types_more');
        if($moreGame != false || empty($recmmendGame)) {
            $res = Db::name('game_types')->field('gametypeid,name,slug,currency,on_nav,isplayable,onhold,country,continent')->where($where)->select();
            $moreGame = [];
            $moreGame['Europe'] = $moreGame['USA'] = $moreGame['Oceania'] = $moreGame['SouthAmerica'] = $moreGame['NorthAmerica'] = $moreGame['Africa'] = $moreGame['Asia'] = [];
            foreach ($res as $k=>$v) {
                if($v['on_nav'] == 1){
                    $moreGame['nav'][] = $v;
                }else{
                    $moreGame['more'][] = $v;
                }
                if($v['continent'] == 'Europe' ){
                    $moreGame['Europe'][] = $v;
                }else if($v['continent'] == 'USA' ){
                    $moreGame['USA'][] = $v;
                }else if($v['continent'] == 'Oceania' ){
                    $moreGame['Oceania'][] = $v;
                }else if($v['continent'] == 'South America' ){
                    $moreGame['SouthAmerica'][] = $v;
                }else if($v['continent'] == 'North America' ){
                    $moreGame['NorthAmerica'][] = $v;
                }else if($v['continent'] == 'Africa'){
                    $moreGame['Africa'][] = $v;
                }else if($v['continent'] == 'Asia'){
                    $moreGame['Asia'][] = $v;
                }
            }
            //print_r($moreGame);exit;
            Cache::set('game_types_more', $moreGame,60*10);
        }
        $this->assign('moreGame',$moreGame);
    }
    public function calculateJackpot($amount)
    {
        if($amount < 1000000){
            $jackpot = $amount;
            $jackpot_unit = '';
        } else if($amount >= 1000000 && $amount < 10000000){
            $jackpot = round($amount/1000000,1);
            $jackpot_unit = Lang::get('million');
        } else {
            $jackpot = round($amount/1000000,0);
            $jackpot_unit = Lang::get('million');
        }
        return $jackpot." ".$jackpot_unit;
    }
    public function recommendArticles($gamgeTypeName='')
    {
        //recommend articles
        $home_page_articles = Cache::get('article_home_page_'.$gamgeTypeName.$this->lang);
        if ($home_page_articles === false) {
            $where = [];
            $where['status'] = 1;
            if($gamgeTypeName != '') {
                $where['gametypes'] = ['like',"%$gamgeTypeName%"];
            }
            $home_page_articles = Db::name('article')
                ->alias('a')
                ->field('a.cover,a.articleid,a.title,a.status,a.update_time,a.sort_order,a.gametypes,a.gametypeids')
                ->where('a.languageid',$this->langid)
                ->where($where)->order('update_time desc')->limit(4)->select();
            if(!empty($home_page_articles)){
                foreach ($home_page_articles as $k=>$v){
                    if(!empty($v['gametypes'])) {
                        $home_page_articles[$k]['gameTypes'] = explode("|", $v['gametypes']);
                    }
                    $home_page_articles[$k]['slug'] = str_replace(" ", "-", $v['title']);
                }
            }
            Cache::set('article_home_page_'.$gamgeTypeName.$this->lang, $home_page_articles,10);
        }
        $this->assign('article_home_page',$home_page_articles);
    }

    public function recommendGameTypes()
    {
        $where = [];
        $where['gt.isplayable'] = 1;
        $where['gt.onhold'] = 0;
        $recommendGame = Db::name('game_types')
            ->alias('gt')
            ->field('gt.gametypeid,gt.name,gt.logo,gt.bg_color,gt.currentjackpot,gt.slug')
            ->order('currentjackpot desc')
            ->limit(4)
            ->where($where)->select();
        if(!empty($recommendGame)){
            foreach ($recommendGame as $k=>$v){
                $recommendGame[$k]['jackpot'] = $this->calculateJackpot($v['currentjackpot']);
            }
        }
        //print_r($recommendGame);exit;
        $this->assign('recommendGame', $recommendGame);
    }

    public function nextTime($time)
    {
        $h = 0;$d = 0;$m = 0;$s = 0;
        $status = $time-time() > 0 ? true : false;
        $d = floor(($time-time())/(3600*24));
        $h = floor(($time-time()-$d*3600*24)/(3600));
        $m = floor(($time-time()-$d*3600*24-$h*3600)/(60));
        $s = ($time-time())%60;
        if($d < 10){
            $d = '0'.$d;
        }
        if($h < 10){
            $h = '0'.$h;
        }
        if($m < 10){
            $m = '0'.$m;
        }
        if($s < 10){
            $s = '0'.$s;
        }
        $data = [];
        $data['d1'] = substr($d,0,1);
        $data['d2'] = substr($d,1,1);
        $data['h1'] = substr($h,0,1);
        $data['h2'] = substr($h,1,1);
        $data['m1'] = substr($m,0,1);
        $data['m2'] = substr($m,1,1);
        $data['s1'] = substr($s,0,1);
        $data['s2'] = substr($s,1,1);
        return ['d'=>$d, 'h'=>$h, 'm'=>$m, 's'=>$s,'data'=>$data, 'status'=>$status];
    }

    private function getUserContry()
    {
        $disablePlayContry = ['US','SG','TW','HK','IN'];//美国、新加坡、台湾、香港、印度
        $ablePlayContry = ['MX'];//墨西哥
        $request = Request::instance();
        //$userip = '56.23.52.41';
        $userip = $request->ip();
        //$userip = '201.116.187.236';
        $gi = null;
        if (!isset($_SESSION['user_country'])) {
            try {
                // Check for cloudflare country header
                $cfCountryHeader = isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : null;
                if (!empty($cfCountryHeader) and strlen($cfCountryHeader) === 2) {
                    $user_country = strtoupper($cfCountryHeader);
                } else {
                    if (is_null($gi)) {
                        $gi = new \GeoIp2\Database\Reader(__DIR__ . '/../../../extend/geoip/GeoLite2-City.mmdb');
                    }
                    $record = $gi->city($userip);
                    $user_country = strtoupper($record->country->isoCode);
                }
            } catch (\Exception $ex) {
                $user_country = '';
            }
            $_SESSION['user_country'] = $user_country;
            return $user_country;
        }else{
            return $_SESSION['user_country'];
        }
    }
}
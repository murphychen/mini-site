<?php

namespace app\index\controller;
use think\Db;

class ResultController extends ControllerInc
{
    public function gameDetailAction()
    {
        $gameSlug = input('param.slug',false);
        $draw = [];
        $game = [];
        $payouts = [];
        $drawList = [];
        if($gameSlug) {
            $game = Db::name('game_types')->where('slug', $gameSlug)->find();
            if(!empty($game)){
                $game['nextdrawtime'] = $this->nextTime(strtotime($game['nextdraw']));
                $game['jackpot'] = $this->calculateJackpot($game['currentjackpot']);
                $slug = $game['slug'];
                $gametypeid = $game['gametypeid'];
                //print_r($game);exit;
                $where = [];
                $where['gameslug'] = $slug;
                $draws = Db::name('games')->where($where)->limit(1000)->order('drawdate desc')->select();
                if(!empty($draws)){
                    foreach ($draws as $k=>$v){
                        $draw = $v;
                        $draw['date'] = date("l jS \of F Y",strtotime($draw['drawdate']));
                        $draw['dateSlug'] = date("Y-m-d",strtotime($draw['drawdate']));
                        $draw['numbers'] = $draw['extranumbers'] = $draw['bonusnumbers'] = $draw['refundnumbers'] = [];
                        if($v['number_str'] != ''){
                            $draw['numbers'] = explode("-",$v['number_str']);
                        }
                        if($v['extranumber_str'] != ''){
                            $draw['extranumbers'] = explode("-",$v['extranumber_str']);
                        }
                        if($v['bonusnumber_str'] != ''){
                            $draw['bonusnumbers'] = explode("-",$v['bonusnumber_str']);
                        }
                        if($v['refundnumber_str'] != ''){
                            $draw['refundnumbers'] = explode("-",$v['refundnumber_str']);
                        }
                        $draw['jackpot'] = $this->calculateJackpot($draw['jackpotsize']);
                        $drawid = $draw['drawid'];
                        $where = [];
                        if (!is_null($draw['batchstr'])) {
                            $where['batchstr'] = $draw['batchstr'];
                        } else {
                            $where['drawid'] = $drawid;
                        }
                        $payouts = Db::name('games_payouttables')->where($where)->order('sortorder asc')->select();
                        $drawList[$k]['draw'] = $draw;
                        $drawList[$k]['payouts'] = $payouts;
                    }
                    $draw = $drawList[0]['draw'];
                    $payouts = $drawList[0]['payouts'];
                }
                $this->assign('draw', $draw);
                $this->assign('game', $game);
                $this->assign('payouts', $payouts);
                $this->assign('drawList', $drawList);
                $this->recommendGameTypes();
                $this->recommendArticles(isset($game['name'])?$game['name']:'');

                $where = [];
                $where['gametypeid'] = $gametypeid;
                $where['status'] = 1;
                $pageContent = Db::name('page_setting')->where('languageid',$this->langid)->where($where)->find();
                //echo Db::name('page_setting')->getLastSql();exit;
                $this->assign('pageContent',$pageContent);

                $this->setHtmlTitle($game['name']);
                return $this->fetch('/result/gamedetail');
            }
            $this->error('404');
        }
    }
    public function indexAction()
    {
        $slug = input('param.slug', false);
        $type = input('param.type', false);
        $date = input('param.date', false);
        $draw = [];
        $game = [];
        $payouts = [];
        if($slug && $date) {
            $game = Db::name('game_types')->where('slug', $slug)->find();
            $game['nextdrawtime'] = $this->nextTime(strtotime($game['nextdraw']));
            $game['jackpot'] = $this->calculateJackpot($game['currentjackpot']);
            $gametypeid = $game['gametypeid'];
            $slug = $game['slug'];
            $where = [];
            $where['gameslug'] = $slug;
            $winNumbers = '';
            if($type) {
                if($type == 'pre'){
                    $draw = Db::name('games')->field('DATE(drawdate) as dateSlug')->where('gameslug', $slug)->where("DATE(drawdate) < '$date'")->limit(1)->order('drawdate desc')->select();
                } else if($type == 'next'){
                    $draw = Db::name('games')->field('DATE(drawdate) as dateSlug')->where('gameslug', $slug)->where("DATE(drawdate) > '$date'")->order('drawdate asc')->limit(1)->select();
                }
                if(!empty($draw)) {
                    $date = $draw[0]['dateSlug'];
                }
                if($this->lang == 'en') {
                    $this->redirect('/'.$slug."/results/".$date);
                }else{
                    $this->redirect('/'.$this->lang.'/'.$slug."/results/".$date);
                }

            } else {
                $draw = Db::name('games')->where('gameslug', $slug)->where("DATE(drawdate) = '$date'")->limit(1)->order('drawid desc')->select();
            }
            if(!empty($draw)){
                $draw[0]['date'] = date("l jS \of F Y",strtotime($draw[0]['drawdate']));
                $draw = $draw[0];
                if($draw['number_str'] != ''){
                    $draw['numbers'] = explode("-",$draw['number_str']);
                }
                if($draw['extranumber_str'] != ''){
                    $draw['extranumbers'] = explode("-",$draw['extranumber_str']);
                }
                if($draw['bonusnumber_str'] != ''){
                    $draw['bonusnumbers'] = explode("-",$draw['bonusnumber_str']);
                }
                if($draw['refundnumber_str'] != ''){
                    $draw['refundnumbers'] = explode("-",$draw['refundnumber_str']);
                }
                $drawid = $draw['drawid'];
                $where = [];
                if (!is_null($draw['batchstr'])) {
                    $where['batchstr'] = $draw['batchstr'];
                } else {
                    $where['drawid'] = $drawid;
                }
                $payouts = Db::name('games_payouttables')->where($where)->order('sortorder asc')->select();
            }
            $this->assign('draw', $draw);
            $this->assign('game', $game);
            $this->assign('date', $date);
            $this->assign('payouts', $payouts);
            $this->recommendGameTypes();

            $where = [];
            $where['gametypeid'] = $gametypeid;
            $where['status'] = 1;
            $pageContent = Db::name('page_setting')->where('languageid',$this->langid)->where($where)->find();
            $this->assign('pageContent',$pageContent);
            if($winNumbers != '') {
                $winNumbers = substr($winNumbers,0,-1);
            }
            $this->setHtmlTitle($game['name'],$date,$winNumbers);
            return $this->fetch();
        }
        $this->error('404');
    }
}
<?php
namespace app\index\controller;
use think\Db;
use think\Cache;
class IndexController extends ControllerInc
{
    public function indexAction()
    {
        //banner
        $banner = Cache::get('banner_home_page');
        if ($banner === false) {
            $list = Db::name('banner')->where('status',1)->order('update_time desc')->limit(1)->select();
            $banner = $list[0];
            //$banner['src'] = urlencode($banner['src']);
            Cache::set('banner_home_page', $banner,60);
        }
        $this->assign('banner',$banner);

        //recommend gameType
        $where = [];
        $where['isplayable'] = 1;
        $where['onhold'] = 0;
        $where['on_banner'] = 1;
        $recmmendGame = Cache::get('game_types_on_banner');
        if($recmmendGame === false || empty($recmmendGame)) {
            $recmmendGame = Db::name('game_types')->where($where)->find();
            if(!empty($recmmendGame)){
                $recmmendGame['jackpot'] = $this->calculateJackpot($recmmendGame['currentjackpot']);
                Cache::set('game_types_on_banner', $recmmendGame,60);
            }else{
                $recmmendGame = [];
            }
        }
        $this->assign('recmmendGame',$recmmendGame);

        //home page game resaults
        $where = [];
        $where['gt.isplayable'] = 1;
        $where['gt.onhold'] = 0;
        $where['gt.on_home_page'] = 1;
        $homePageGame = Cache::get('game_types_on_home_page');
        if($homePageGame === false || empty($homePageGame)) {
            $homePageGame = Db::name('game_types')
                    ->alias('gt')
                    ->where($where)->select();
            $gameTypeIds = [];
            foreach ($homePageGame as $k=>$v) {
                $gameTypeIds[] = $v['gametypeid'];
                $homePageGame[$k]['jackpot'] = $this->calculateJackpot($v['currentjackpot']);
                $where = [];
                $where['gameslug'] = $v['slug'];
                $draw = Db::name('games')
                    ->where($where)->limit(1)->order('drawdate desc')->select();
                if(!empty($draw)) {
                    $draw = $draw[0];
                    if($draw['number_str'] != ''){
                        $draw['numbers'] = explode("-",$draw['number_str']);
                    }
                    if($draw['extranumber_str'] != ''){
                        $draw['extranumbers'] = explode("-",$draw['extranumber_str']);
                    }
                    if($draw['refundnumber_str'] != ''){
                        $draw['bonusnumbers'] = explode("-",$draw['refundnumber_str']);
                    }
                    $homePageGame[$k]['draw'] = $draw;
                    $homePageGame[$k]['draw']['date'] = date("l jS \of F Y",strtotime($draw['drawdate']));
                    $homePageGame[$k]['draw']['jackpot'] = $this->calculateJackpot($draw['jackpotsize']);

                } else {
                    $homePageGame[$k]['draw'] = [];
                }
            }
            Cache::set('game_types_on_home_page', $homePageGame,60);
        }
        $this->assign('homePageGame',$homePageGame);
        $where = [];
        $where['gametypeid'] = 0;
        $where['status'] = 1;
        $homeContent = Db::name('page_setting')->where('languageid',$this->langid)->where($where)->find();
        $this->assign('homeContent',$homeContent);
        $this->recommendArticles();
        return $this->fetch();
    }
}

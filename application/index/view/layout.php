<!DOCTYPE html>
<html lang="{$langHead}">
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8" />
    <meta name="description" content="{$description}">
    <link rel="alternate" hreflang="en" href="http://www.lotto-vm.com{$params_str}" />
    <link rel="alternate" hreflang="de" href="http://www.lotto-vm.com/de{$params_str}" />
    <link rel="alternate" hreflang="fi" href="http://www.lotto-vm.com/fi{$params_str}" />
    <link rel="alternate" hreflang="sv" href="http://www.lotto-vm.com/sv{$params_str}" />
    <link rel="alternate" hreflang="no" href="http://www.lotto-vm.com/no{$params_str}" />
    <title>{$title}{$lang}</title>
    <link rel="stylesheet" href="/__PUBLIC__css/satellite.css">
    <script src="/__PUBLIC__js/jquery-3.2.1.min.js"></script>
</head>
<body>
<!-- header start  -->
<!-- 页面滚动就加上satel-head-scroll -->
<div class="satel-header  ">
    <div class="satel-header-cont clearfix">
        <div class="satel-logo">
        	{if condition="empty($lang)"}
        	<a href="/" target="_self">
        	{else /}
            <a href="{$lang}" target="_self">
            {/if}
                {if condition="$controller != 'index' || ($controller=='result' && $action='gamedetail')"}
                <img src="/__PUBLIC__images/satellite/logo-b.png" alt="">
                {else /}
                <img src="/__PUBLIC__images/satellite/logo.png" alt="">
                {/if}
            </a>
            <input type="hidden" id="controllerName" value="{$controller}">
            <input type="hidden" id="actionName" value="{$action}">
            <input type="hidden" id="logo-b-url" value="/__PUBLIC__images/satellite/logo-b.png">
            <input type="hidden" id="logo-url" value="/__PUBLIC__images/satellite/logo.png">
            <input type="hidden" id="js-lang" value="{$lang}">
        </div>
        <div class="satel-head-nav">
            <ul class="shn-list">
                {foreach name="moreGame.nav" item="vo" }
                <li class="shn-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                {/foreach}
                <li class="shn-item shn-item-more">
                    <a href="">{:lang('more_lotteries')}
                        <span class="shn-item-drop"></span>
                    </a>
                    <!-- more lotteries -->
                    <div class="more-lotto-wrap hide">
                        <div class="more-lotto-cont clearfix">
                            <div class="mlc-wrap mlc-wrap30 mr10 fl">
                                <div class="mlc-tit">{:lang('europe')}</div>
                                <div class="mlc-cont clearfix mlc-main">
                                    <ul class="mlc-cont-list ">
                                        {foreach name="moreGame.Europe" item="vo" key="k"}
                                        {if condition="$k < 20"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                    <ul class="mlc-cont-list ">
                                        {foreach name="moreGame.Europe" item="vo" key="k"}
                                        {if condition="$k >= 20 && $k < 40"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul> 
                                    <ul class="mlc-cont-list ">
                                        {foreach name="moreGame.Europe" item="vo" key="k"}
                                        {if condition="$k >= 40 && $k < 60"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul> 
                                </div>
                            </div>
                            <div class="mlc-wrap mlc-wrap20 fl">
                                <div class="mlc-tit">{:lang('oceania')}</div>
                                <div class="mlc-cont">
                                    <ul class="mlc-cont-list ">
                                        {foreach name="moreGame.Oceania" item="vo" key="k"}
                                        {if condition="$k < 10"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                            <div class="mlc-wrap mlc-wrap20 fl">
                                <div class="mlc-tit">{:lang('south_america')}</div>
                                <div class="mlc-cont">
                                    <ul class="mlc-cont-list ">
                                        {foreach name="moreGame.SouthAmerica" item="vo" key="k"}
                                        {if condition="$k < 10"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                            <div class="mlc-wrap mlc-wrap20 fl">
                                <div class="mlc-tit">{:lang('north_america')}</div>
                                <div class="mlc-cont">
                                    <ul class="mlc-cont-list ">
                                        {foreach name="moreGame.NorthAmerica" item="vo" key="k"}
                                        {if condition="$k < 10"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                            <div class="mlc-wrap mlc-wrap20 fl">
                                <div class="mlc-tit">{:lang('usa')}</div>
                                <div class="mlc-cont">
                                    <ul class="mlc-cont-list">
                                        {foreach name="moreGame.USA" item="vo" key="k"}
                                        {if condition="$k < 10"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                            <div class="mlc-wrap mlc-wrap20 fl">
                                <div class="mlc-tit">{:lang('africa')}</div>
                                <div class="mlc-cont">
                                    <ul class="mlc-cont-list">
                                        {foreach name="moreGame.Africa" item="vo" key="k"}
                                        {if condition="$k < 10"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                            <div class="mlc-wrap mlc-wrap20 fl">
                                <div class="mlc-tit">{:lang('asia')}</div>
                                <div class="mlc-cont">
                                    <ul class="mlc-cont-list">
                                        {foreach name="moreGame.Asia" item="vo" key="k"}
                                        {if condition="$k < 10"}
                                        <li class="mlc-cont-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                                        {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /more lotteries -->
                </li>
            </ul>
        </div>
        <div class="satel-head-r">
            <div class="satel-head-app">
                {if condition="$isPlayContry === true"}
                <a href="https://www.multilotto.com/en/campaign/landing/app-download" target="_blank">
                    {:lang('download_app')}
                    <span class="sha-icon"><i></i></span>
                </a>
                {/if}
            </div>
            <div class="lan-cont">
                <div class="lan-flag {$logo_class}"></div>
                <div class="lan-drop"></div>
                <!-- layer  language -->
                <div class="more-lotto-wrap lan-layer hide" style="height: auto;">
                    <div class="more-lotto-cont clearfix">
                        <div class="mlc-wrap  ">
                            <div class="mlc-tit">{$languages.en.name}</div>
                            <div class="mlc-cont">
                                <ul  class="mlc-cont-list ">
                                {foreach name="languages" item="vo"}
                                {if condition="'/'.$vo.logo == $lang"}
                                    <li class='mlc-cont-item js-change-lang on'>
                                {else /}
                                    <li class='mlc-cont-item js-change-lang'>
                                {/if}
                                <a href="javascript:" onclick="changeLang('{$vo.locale}')"  >
                                    {$vo.name}
                                </a>
                                </li>
                                {/foreach}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  /layer language -->
            </div>
        </div>
    </div>
</div>
<!-- header end -->
<!-- header start  -->
<div class="satel-head-scroll header-h5  l-full   ">
    <div class="satel-header-cont clearfix">
        <!-- 如果被选中了 加上class tab-nav-cur -->
        <div class="tab-nav  l-box-center "><span></span></div>
        <div class="satel-logo">
            <a href="/">
                <img src="/__PUBLIC__images/satellite/logo-b.png" alt="">
            </a>
        </div>
        <div class="satel-head-r">
            <div class="satel-head-app">
                <span class="sha-icon"></span>
            </div>
        </div>
    </div>
</div>

<div class="satel-head-nav satel-mobile-nav ">
    <!-- 第一层菜单-->
    <ul class="shn-list ">
        {foreach name="moreGame.nav" item="vo" }
        <li class="shn-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
        {/foreach}
        <li class="shn-item shn-item-more"><a href="">{:lang('more_lotteries')} <span class="shn-item-drop"></span></a>
        </li>
    </ul>
    <!-- 更多彩种 -->
    <div class="nav-more-lotto   l-flex-column hide">
        <div class="satel-header-cont clearfix l-box-vertical-center">
            <div class=" back-to-nav  "><span class="shn-item-drop"></span>{:lang('back')}</div>
        </div>
        <div class="l-flex-1 l-relative ">
            <ul class="shn-list ">
                {foreach name="moreGame.more" item="vo" }
                <li class="shn-item"><a href="{$lang}/{$vo.slug}">{$vo.name}</a></li>
                {/foreach}
            </ul>
        </div>
    </div>
    <!-- 语言选择-->
    <div class="nav-more-lan   l-flex-column hide ">
        <div class="satel-header-cont clearfix l-box-vertical-center">
            <div class="back-to-nav  "><span class="shn-item-drop"></span>{:lang('back')}</div>
        </div>
        <div class="l-flex-1 l-relative ">
            <ul class="shn-list ">
                {foreach name="languages" item="vo"}
                <li class="shn-item">
                    <a  class="l-flex-row l-box-vertical-center " href="javascript:" onclick="changeLang({$vo.locale})">
                        <div class="shn-item-flag l-box-center"><span class="es"></span></div>
                        <div class="l-flex-1"> {$vo.name}</div>
                    </a>
                </li>
                {/foreach}
            </ul>
        </div>
    </div>
</div>
<!-- header end -->
{if condition="$controller == 'index'"}
<div class="satel-wrap">
    {elseif condition="$controller == 'result' && $action == 'gameDetail'" /}
    <div class="satel-wrap satel-num-wrap">
        {else /}
        <div class="satel-wrap satel-news-wrap">
            {/if}
            {__CONTENT__}
            <!-- footer  start -->
            <div class="satel-footer">
                <div class="satel-footer-limit">
                    <span>18+</span>
                </div>
                <div class="datel-footer-copyright">
                    Material Copyright &copy; 2018 xxx.com
                </div>
            </div>
            <!-- footer  end -->

        </div>

        <script>
            // scroll
            var controllerName = $("#controllerName").val();
            var actionName = $("#actionName").val();
            var logo_b_url = $("#logo-b-url").val();
            var logo_url = $("#logo-url").val();
            var distance = 0;
            var changeStatus = false;
            if(controllerName == 'index' || (controllerName == 'result' && actionName=='gamedetail')){
                changeStatus = true;
            }
            if(changeStatus) {
                $(document).scroll(function () {
                    distance = $(document).scrollTop();
                    if (distance == 0) {
                        $('.satel-logo img').attr('src',logo_url);
                        $('.satel-header').removeClass("satel-head-scroll");
                    } else {
                        $('.satel-logo img').attr('src',logo_b_url);
                        $('.satel-header').addClass("satel-head-scroll");
                    }
                })
            } else {
                $('.satel-header').addClass("satel-head-scroll");
            }
            // more
            $(".shn-item-more").mouseover(function(){
                $(this).children(".more-lotto-wrap").removeClass("hide");
                $(this).parents(".satel-header").addClass("satel-head-scroll");
                $('.satel-logo img').attr('src',logo_b_url);
                $(".shn-item-more").mouseout(function(){
                    $(this).children(".more-lotto-wrap").addClass("hide");
                    if(changeStatus && distance == 0) {
                        $('.satel-logo img').attr('src',logo_url);
                        $(this).parents(".satel-header").removeClass("satel-head-scroll");
                    } else {
                        $(this).parents(".satel-header").addClass("satel-head-scroll");
                        $('.satel-logo img').attr('src',logo_b_url);
                    }
                })
            })
            // change lang
            $(".lan-cont").mouseover(function(){
                $(this).children(".lan-layer").removeClass("hide");
                $(this).parents(".satel-header").addClass("satel-head-scroll");
                $('.satel-logo img').attr('src',logo_b_url);
                $(".lan-cont").mouseout(function(){
                    $(this).children(".lan-layer").addClass("hide");
                    if(changeStatus && distance == 0) {
                        $('.satel-logo img').attr('src',logo_url);
                        $(this).parents(".satel-header").removeClass("satel-head-scroll");
                    } else {
                        $(this).parents(".satel-header").addClass("satel-head-scroll");
                        $('.satel-logo img').attr('src',logo_b_url);
                    }
                })
            })

            // change nav
            $(".tab-nav").click(function(){
                $(this).addClass("tab-nav-cur");
                $(".satel-mobile-nav").css("display","block");
                $(".shn-item-more").click(function(){
                    $(".nav-more-lotto").removeClass("hide");
                    $(".tab-nav").click(function(){
                        $(this).removeClass("tab-nav-cur");
                        $(".satel-mobile-nav").css("display","none");
                    })

                    $(".satel-header-cont").click(function(){
                        $(this).parent(".nav-more-lotto").addClass("fadeOut");
                        setTimeout($(this).parent(".nav-more-lotto").addClass("hide"),1200);

                    })
                })
            });

            function changeLang(locale) {
                var pathname = document.location.pathname;
                $.ajax({
                    url:'/lang/changeLocale',
                    type:'post',
                    data:{
                        locale:locale,
                        pathname:pathname
                    },
                    success:function (res) {
                        window.location.href = res.url;
                    }
                })
            }

        </script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122650874-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-122650874-1');
        </script>
</body>
</html>

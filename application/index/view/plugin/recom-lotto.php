<!-- 文章中间的彩种推荐 -->
<div class="article-recom-lotto clearfix">
    <div class="count-time-cont">
        <div class="ctc-tit">{:lang('new_draw')}</div>
        <div class="ctc-list">
            <ul class="ctc-list-box clearfix">
                <li class="ctc-item js-days">
                    <span class="ctc-num-box">0</span>
                    <span class="ctc-num-box">2</span>
                    <div class="ctc-time">{:lang('days')}</div>
                </li>
                <li class="ctc-item js-hours">
                    <span class="ctc-num-box">1</span>
                    <span class="ctc-num-box">5</span>
                    <div class="ctc-time">{:lang('hours')}</div>
                </li>
                <li class="ctc-item js-minutes">
                    <span class="ctc-num-box">4</span>
                    <span class="ctc-num-box">6</span>
                    <div class="ctc-time">{:lang('minutes')}</div>
                </li>
                <li class="ctc-item js-seconds">
                    <span class="ctc-num-box ctc-num-red">5</span>
                    <span class="ctc-num-box ctc-num-red">9</span>
                    <div class="ctc-time">{:lang('seconds')}</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="recom-next">
        <div class="recom-next-jac">{:lang('next_jackpot')}</div>
        <div class="recom-jac-num">€45 {:lang('miliion')}</div>
        <div class="button-bt1">{:lang('play_now')}</div>
    </div>
</div>
<script>
    var d1, d2, h1, h2, m1, m2, s1, s2;
    var time = 122554;
    $(function () {
        if(d1){
            setInterval(changeTime,1000);
        }
    });
    function changeTime() {
        var d,h,m,s;
        d = Math.floor(time/3600*24);
        h = Math.floor((time-d*3600*24)/3600);
        m = Math.floor((time-d*3600*24-h*3600)/60);
        s = time%60;
        if(d < 10){
            d1 = 0;d2 = d;
        } else {
            d1 = d.substring(0,1);d2 = d.substring(1,1);
        }
        if(h < 10){
            h1 = 0;h2 = h;
        } else {
            h1 = h.substring(0,1);h2 = h.substring(1,1);
        }
        if(m < 10){
            m1 = 0;m2 = m;
        } else {
            m1 = m.substring(0,1);m2 = m.substring(1,1);
        }

        if(h < 10){
            h1 = 0;h2 = h;
        } else {
            h1 = h.substring(0,1);h2 = h.substring(1,1);
        }

        $('.js-days span').eq(0).html(d1);
        $('.js-days span').eq(1).html(d2);
        $('.js-hours span').eq(0).html(h1);
        $('.js-hours span').eq(1).html(h2);
        $('.js-minutes span').eq(0).html(m1);
        $('.js-minutes span').eq(1).html(m2);
        $('.js-seconds span').eq(0).html(s1);
        $('.js-seconds span').eq(1).html(s2);
    }
</script>
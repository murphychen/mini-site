<div class="satel-cont">
    <div class="prize-wrap clearfix">
        <!-- 左边的开奖信息 -->
        <div class="prize-num-l">
            <!-- 开奖的奖池获奖人数信息 -->
            <div class="prize-break">
                <h1 class="satel-news-tit">{:lang('lotto_news')}</h1>
            </div>
            <!-- 单条新闻 -->
            {foreach name="last_articles" item="vo"}
            <div class="lotto-news-box clearfix">
                <a href="{$lang}/articles/{$vo.slug}">
                <div class="lotto-news-pic">
                    <img src="{$vo.cover}"  alt="{$vo.title}">
                </div>
                <div class="lotto-news-deta ">
                    <div class="lnd-tit">{$vo.title}</div>
                    <div class="lnd-abs">{$vo.intro}
                    </div>
                    <div class="lnd-signs lotto-news-sign">
                        <ul class="clearfix">
                            {if condition="!empty($vo.gameTypes)"}
                            {foreach name="vo.gameTypes" item="v"}
                            <li>{$v}</li>
                            {/foreach}
                            {/if}
                        </ul>
                    </div>
                </div>
                </a>
            </div>
            {/foreach}
        </div>
        <!-- 右边的推广 -->
        <div class="prize-num-r">
            <!-- 默认的广告图 -->
            <div class="help-user-buy">
                <img src="/__PUBLIC__images/satellite/add-b.png" alt="">
            </div>
            <!-- 推荐彩种1 /4-->
            {foreach name="recommendGame" item="vo"}
            <div class="recom-lotto-box">
                <div class="rexom-lotto-head  {$vo.bg_color}-bg">
                    <div class="lotto-icon">
                        <img src="{$vo.logo}" alt="{$vo.name}">
                    </div>
                </div>
                <div class="recom-next">
                    <div class="recom-next-jac">{:lang('next_jackpot')}</div>
                    <div class="recom-jac-num">€{$vo.jackpot}</div>
                    {if condition="$isPlayContry === true && in_array($vo.slug,[
                        'usa-powerball',
                        'eurojackpot',
                        'mega-millions',
                        'super-ena-lotto',
                        'california-super-lotto',
                        'euromillions',
                        'germany-lotto',
                        'la-primitiva',
                        'el-gordo',
                        'cash4life',
                        'finland-lotto',
                        'canada-lotto-649',
                        'new-york-lotto',
                        'poland-lotto',
                        'irish-lotto',
                        'norway-lotto',
                        'mega-sena',
                        'bonoloto',
                        'austria-lotto',
                    ])"}
                    <a href="https://www.multilotto.com/{$langHead}/{$vo.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {else /}
                    <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {/if}
                </div>
            </div>
            {/foreach}
        </div>
    </div>
</div>
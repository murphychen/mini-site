<div class="satel-cont">
    <div class="prize-wrap clearfix">
        <!-- 左边的开奖信息 -->
        <div class="prize-num-l">
            <!-- 开奖的奖池获奖人数信息 -->
            <div class="prize-break prize-break-article">
                <h1 class="satel-news-tit ">{$detail.title}</h1>
                <div class="lnd-signs">
                    <ul class="clearfix">
                        {if condition="!empty($detail.gameTypes)"}
                        {foreach name="detail.gameTypes" item="vo"}
                        <li>{$vo}</li>
                        {/foreach}
                        {/if}
                        <li>big jackpot</li>
                    </ul>
                </div>
            </div>
            <div class="satel-check-cont  satel-article-cont clearfix">
                <!-- 文字和图片流式布局 -->
                {$detail.content}
            </div>
            <!-- How to Check Lotto Results  end -->
        </div>
        <!-- 右边的推广 -->
        <div class="prize-num-r">
            <!-- 默认的广告图 -->
            <div class="help-user-buy">
                <img src="/__PUBLIC__images/satellite/add-b.png" alt="">
            </div>
            <!-- 推荐彩种1 /2-->
            {foreach name="recommendGame" item="vo"}
            <div class="recom-lotto-box">
                <div class="rexom-lotto-head  {$vo.bg_color}-bg">
                    <div class="lotto-icon">
                        <img src="{$vo.logo}" alt="{$vo.name}">
                    </div>
                </div>
                <div class="recom-next">
                    <div class="recom-next-jac">Next Jackpot</div>
                    <div class="recom-jac-num">€{$vo.jackpot}</div>
                    {if condition="$isPlayContry === true && in_array($vo.slug,[
                        'usa-powerball',
                        'eurojackpot',
                        'mega-millions',
                        'super-ena-lotto',
                        'california-super-lotto',
                        'euromillions',
                        'germany-lotto',
                        'la-primitiva',
                        'el-gordo',
                        'cash4life',
                        'finland-lotto',
                        'canada-lotto-649',
                        'new-york-lotto',
                        'poland-lotto',
                        'irish-lotto',
                        'norway-lotto',
                        'mega-sena',
                        'bonoloto',
                        'austria-lotto',
                    ])"}
                    <a href="https://www.multilotto.com/{$langHead}/{$vo.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {else /}
                    <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {/if}
                </div>
            </div>
            {/foreach}
        </div>
    </div>
    <!-- Lotto News  start -->
    <div class="satel-lotto-news">
        <div class="satel-news-cont">
            <ul class="snc-list clearfix">
                {foreach name="article_home_page" item="vo"}
                <li class="snc-item clearfix">
                    <a href="{$lang}/articles/{$vo.slug}">
                        <div class="snc-item-img">
                            <img src="{$vo.cover}"  alt="">
                        </div>
                        <div class="snc-item-detail">
                            <p>{$vo.title}</p>
                            <div class="lnd-signs  snc-item-signs clearfix">
                                <ul>
                                    {if condition="!empty($vo.gameTypes)"}
                                    {foreach name="vo.gameTypes" item="v"}
                                    <li>{$v}</li>
                                    {/foreach}
                                    {/if}
                                </ul>
                            </div>
                        </div>
                    </a>
                </li>
                {/foreach}
            </ul>
        </div>
    </div>
    <!-- Lotto News  end -->
</div>
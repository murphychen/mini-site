<div class="banner-wrap">
    <input type="hidden" id="banner-src" value="{$banner.src}">
    {if condition="!empty($recmmendGame)"}
    <div class="banner-cont">
        <div class="lotto-icon">
            <img src="{$recmmendGame.logo}" alt="{$recmmendGame.name}">
        </div>
        <div class="lotto-jackpot">€{$recmmendGame.jackpot}</div>
        <div class="lotto-next">{:lang('next_jackpot')}</div>
        {if condition="$isPlayContry === true && in_array($recmmendGame.slug,[
            'usa-powerball',
            'eurojackpot',
            'mega-millions',
            'super-ena-lotto',
            'california-super-lotto',
            'euromillions',
            'germany-lotto',
            'la-primitiva',
            'el-gordo',
            'cash4life',
            'finland-lotto',
            'canada-lotto-649',
            'new-york-lotto',
            'poland-lotto',
            'irish-lotto',
            'norway-lotto',
            'mega-sena',
            'bonoloto',
            'austria-lotto',
        ])"}
        <a href="https://www.multilotto.com/{$langHead}/{$recmmendGame.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
        {else /}
        <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
        {/if}
    </div>
    {/if}
</div>
<div class="satel-cont">
    <!-- check lotto results  start -->
    <div class="satel-lotto-result">
        <h1 class="satel-result-tit">{:lang('check_lotto_result_online')}</h1>
        <div class="satel-result-cont">
            <ul class="satel-result-list clearfix">
                {foreach name="homePageGame" item="vo"}
                <li class="satel-result-item {$vo.bg_color}-bg js-result-index" href="{$lang}/{$vo.slug}">
                    <div class="satel-item-l">
                        <div class="lotto-icon">
                            <img src="{$vo.logo}" alt="{$vo.name}">
                        </div>
                        <div class="button-bt2"><a href="{$lang}/{$vo.slug}">{:lang('prize_breakdown')}</a></div>
                    </div>
                    <div class="satel-item-r">
                        <div class="satel-item-time">{if condition="isset($vo.draw.date)"} {$vo.draw.date}{/if}<!--Friday 18 May 2018--></div>
                        <div class="satel-item-ball">
                            <ul class="satel-ball clearfix">
                                {if condition="!empty($vo.draw.numbers)"}
                                {foreach name="$vo.draw.numbers" item="num"}
                                <li class="satel-baller">{$num}</li>
                                {/foreach}
                                {/if}
                                {if condition="!empty($vo.draw.extranumbers)"}
                                {foreach name="$vo.draw.extranumbers" item="num"}
                                <li class="satel-baller extra-num ">{$num}</li>
                                {/foreach}
                                {/if}
                                {if condition="!empty($vo.draw.bonusnumbers)"}
                                {foreach name="$vo.draw.bonusnumbers" item="num"}
                                <li class="satel-baller bonus-num ">{$num}</li>
                                {/foreach}
                                {/if}
                            </ul>
                        </div>
                        <div class="satel-item-jackpot">{:lang('jackpot')}: €{if condition="isset($vo.draw.jackpot)"} {$vo.draw.jackpot}{/if}</div>
                    </div>
                </li>
                {/foreach}
            </ul>
        </div>
    </div>
    <!-- check lotto results  end -->

    <!-- How to Check Lotto Results  start -->
    {if condition="!empty($homeContent)"}
    <div class="satel-lotto-check">
        <h2 class="satel-check-tit">{:lang('how_to_check_lotto_results')}</h2>
        <div class="satel-check-cont clearfix">
            {$homeContent.content}
        </div>
    </div>
    {/if}
    <!-- How to Check Lotto Results  end -->

    <!-- Lotto News  start -->
    <div class="satel-lotto-news">
        <h2 class="satel-news-tit">{:lang('lotto_news')}
            <div class="snt-more js-more-article">
                <a href="{$lang}/articles" style="color: black">
                {:lang('more')}
                <div class="snt-more-icon"></div>
                </a>
            </div>
        </h2>
        <div class="satel-news-cont">
            <ul class="snc-list clearfix">
                {foreach name="article_home_page" item="vo"}
                <li class="snc-item clearfix">
                    <a href="{$lang}/articles/{$vo.slug}">
                        <div class="snc-item-img">
                            <img src="{$vo.cover}"  alt="">
                        </div>
                        <div class="snc-item-detail">
                            <p>{$vo.title}</p>
                            <div class="lnd-signs  snc-item-signs clearfix">
                                <ul>
                                    {if condition="!empty($vo.gameTypes)"}
                                    {foreach name="vo.gameTypes" item="v"}
                                    <li>{$v}</li>
                                    {/foreach}
                                    {/if}
                                </ul>
                            </div>
                        </div>
                    </a>
                </li>
                {/foreach}
            </ul>
        </div>
    </div>
    <!-- Lotto News  end -->
</div>
<script>
    $(function () {
        var src = encodeURI($('#banner-src').val());
        $(".banner-wrap").css({"background-image":"url('"+ src +"')"});
        
        $(".js-more-article").on('click', function () {
            //window.location.href = 'articles';
        })
        $(".js-result-index").on('click', function () {
            var href = $(this).attr('href');
            window.location.href = href;
        })
    })
</script>
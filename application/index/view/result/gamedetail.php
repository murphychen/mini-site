<div class="banner-wrap banner-wrap-num">
    <!-- 选号页面 start -->
    <div class="pick-num-wrap">
        <h1 class="pick-num-tit">{$game.name} {:lang('results_and_winning_numbers')}</h1>
        <div class="pick-num-cont clearfix">
            <!-- 左边的倒计时 start -->
            <div class="pnc-l">
                <div class="draw-time-cont">
                    <div class="satel-result-item powerball-bg">
                        <div class="satel-item-l">
                            <div class="lotto-icon">
                                <img src="{$game.logo}" alt="{$game.name}">
                            </div>
                        </div>

                        <div class="satel-item-r">
                            {if condition="!empty($draw)"}
                            <div class="satel-item-time">{$draw.date}</div>
                            <div class="satel-item-ball">
                                <ul class="satel-ball clearfix">
                                    {foreach name="$draw.numbers" item="num"}
                                    <li class="satel-baller">{$num}</li>
                                    {/foreach}
                                    {if condition="!empty($draw.extranumbers)"}
                                    {foreach name="$draw.extranumbers" item="num"}
                                    <li class="satel-baller extra-num ">{$num}</li>
                                    {/foreach}
                                    {/if}
                                    {if condition="!empty($draw.bonusnumbers)"}
                                    {foreach name="$draw.bonusnumbers" item="num"}
                                    <li class="satel-baller bonus-num ">{$num}</li>
                                    {/foreach}
                                    {/if}

                                    {if condition="!empty($draw.refundnumbers)"}
                                    {foreach name="$draw.refundnumbers" item="num"}
                                    <li class="satel-baller">{$num}</li>
                                    {/foreach}
                                    {/if}
                                </ul>
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>
                <!-- 倒计时 -->
                <div class="count-time-cont">
                    <div class="ctc-tit">{:lang('new_drawing')}</div>
                    <div class="ctc-list">
                        {if condition="$game.nextdrawtime.status === true"}
                        <ul class="ctc-list-box clearfix">
                            <li class="ctc-item js-days">
                                <span class="ctc-num-box">{$game.nextdrawtime.data.d1}</span>
                                <span class="ctc-num-box">{$game.nextdrawtime.data.d2}</span>
                                <div class="ctc-time">{:lang('days')}</div>
                            </li>
                            <li class="ctc-item js-hours">
                                <span class="ctc-num-box">{$game.nextdrawtime.data.h1}</span>
                                <span class="ctc-num-box">{$game.nextdrawtime.data.h2}</span>
                                <div class="ctc-time">{:lang('hours')}</div>
                            </li>
                            <li class="ctc-item js-minutes">
                                <span class="ctc-num-box">{$game.nextdrawtime.data.m1}</span>
                                <span class="ctc-num-box">{$game.nextdrawtime.data.m2}</span>
                                <div class="ctc-time">{:lang('minutes')}</div>
                            </li>
                            <li class="ctc-item js-seconds">
                                <span class="ctc-num-box ctc-num-red">{$game.nextdrawtime.data.s1}</span>
                                <span class="ctc-num-box ctc-num-red">{$game.nextdrawtime.data.s2}</span>
                                <div class="ctc-time">{:lang('seconds')}</div>
                            </li>
                        </ul>
                        {/if}
                    </div>
                    <div class="ctc-next-jac">{:lang('next_jackpot')}: €{$game.jackpot}</div>
                </div>
                <!-- /倒计时 -->
            </div>
            <!-- 左边的倒计时 end -->

            <!-- 右边的快速选号 start  -->
            <div class="pnc-r ">
                <div class="quick-pick-name">{:lang('quick_pick')}</div>
                <div class="quick-pick-wrap">
                    <div class="quick-pick-way">{:lang('select_numbers')}</div>
                    <div class="quick-pick-box">
                        <ul class="qucik-pick-list">
                            <!-- 如果是最后一个 就加上quick-mobile-pick -->
                            <li class="quick-pick-item clearfix ">
                                <div class="button-bt3">{:lang('quick_pick')}</div>
                                <ul class="qpc-num-list clearfix">
                                    <li class="qpc-num-item">6</li>
                                    <li class="qpc-num-item">1</li>
                                    <li class="qpc-num-item">8</li>
                                    <li class="qpc-num-item">2</li>
                                    <li class="qpc-num-item">12</li>
                                    <li class="qpc-num-item  extra-num">65</li>
                                    <li class="qpc-num-item bonus-num">48</li>
                                </ul>
                                <div class="qpc-num-del"></div>
                            </li>
                            <li class="quick-pick-item clearfix">
                                <div class="button-bt3">{:lang('quick_pick')}</div>
                                <ul class="qpc-num-list clearfix">
                                    <li class="qpc-num-item">6</li>
                                    <li class="qpc-num-item">1</li>
                                    <li class="qpc-num-item">8</li>
                                    <li class="qpc-num-item">2</li>
                                    <li class="qpc-num-item">12</li>
                                    <li class="qpc-num-item  extra-num">65</li>
                                    <li class="qpc-num-item bonus-num">48</li>
                                </ul>
                                <div class="qpc-num-del"></div>
                            </li>
                            <li class="quick-pick-item clearfix">
                                <div class="button-bt3">{:lang('quick_pick')}</div>
                                <ul class="qpc-num-list clearfix">
                                    <li class="qpc-num-item">6</li>
                                    <li class="qpc-num-item">1</li>
                                    <li class="qpc-num-item">8</li>
                                    <li class="qpc-num-item">2</li>
                                    <li class="qpc-num-item">12</li>
                                    <li class="qpc-num-item  extra-num">65</li>
                                    <li class="qpc-num-item bonus-num">48</li>
                                </ul>
                                <div class="qpc-num-del"></div>
                            </li>
                            <li class="quick-pick-item clearfix">
                                <div class="button-bt3">{:lang('quick_pick')}</div>
                                <ul class="qpc-num-list clearfix">
                                    <li class="qpc-num-item">6</li>
                                    <li class="qpc-num-item">1</li>
                                    <li class="qpc-num-item">8</li>
                                    <li class="qpc-num-item">2</li>
                                    <li class="qpc-num-item">12</li>
                                    <li class="qpc-num-item  extra-num">65</li>
                                    <li class="qpc-num-item bonus-num">48</li>
                                </ul>
                                <div class="qpc-num-del"></div>
                            </li>
                            <li class="quick-pick-item  quick-mobile-pick clearfix">
                                <div class="button-bt3">{:lang('quick_pick')}</div>
                                <ul class="qpc-num-list clearfix">
                                    <li class="qpc-num-item"></li>
                                    <li class="qpc-num-item"></li>
                                    <li class="qpc-num-item"></li>
                                    <li class="qpc-num-item"></li>
                                    <li class="qpc-num-item"></li>
                                    <li class="qpc-num-item  extra-num"></li>
                                    <li class="qpc-num-item bonus-num"></li>
                                </ul>
                                <div class="qpc-num-del"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="qpc-but clearfix">
                        <div class="button-bt3">{:lang('pick_1_draws')}</div>
                        {if condition="$isPlayContry === true && in_array($game.slug,[
                            'usa-powerball',
                            'eurojackpot',
                            'mega-millions',
                            'super-ena-lotto',
                            'california-super-lotto',
                            'euromillions',
                            'germany-lotto',
                            'la-primitiva',
                            'el-gordo',
                            'cash4life',
                            'finland-lotto',
                            'canada-lotto-649',
                            'new-york-lotto',
                            'poland-lotto',
                            'irish-lotto',
                            'norway-lotto',
                            'mega-sena',
                            'bonoloto',
                            'austria-lotto',
                        ])"}
                        <a href="https://www.multilotto.com/{$langHead}/{$game.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                        {else /}
                        <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                        {/if}
                    </div>
                </div>
            </div>
            <!-- 右边的快速选号 end  -->
        </div>
    </div>
</div>
<div class="satel-cont  ">
    <div class="prize-wrap clearfix">
        <!-- 左边的开奖信息 -->
        <div class="prize-num-l">
            <!-- 开奖的奖池获奖人数信息 -->
            <div class="prize-break">
                <h2 class="satel-news-tit">{:lang('prize_breakdown')}</h2>
                <ul class="pb-tb">
                    <li class="pb-tb-tr clearfix  pb-tb-tit">
                        <ul class="pb-tr-list clearfix">
                            <!--<li class="pb-tr-item">{:lang('divisions')}</li>-->
                            <li class="pb-tr-item">{:lang('match')}</li>
                            <li class="pb-tr-item">{:lang('prize_money')}</li>
                            <li class="pb-tr-item">{:lang('Winners')}</li>
                            <li class="pb-tr-item">{:lang('winning_odds')}</li>
                        </ul>
                    </li>
                    {foreach name="payouts" item="vo"}
                    <li class="pb-tb-tr clearfix">
                        <ul class="pb-tr-list  clearfix">
                            <!--<li class="pb-tr-item">{$vo.divisions}</li>-->
                            <li class="pb-tr-item">{$vo.match}</li>
                            <li class="pb-tr-item">
                            	{if condition="$vo.prize_money=='No winner'"}
                            	{$vo.prize_money}
                            	{else /}
                            	€{$vo.prize_money}
                            	{/if}
                            </li>
                            <li class="pb-tr-item">
                                {if condition="!is_null($vo.winners)"}
                                {$vo.winners}
                                {else /}
                                --
                                {/if}
                            </li>
                            <li class="pb-tr-item">
                                {if condition="!empty($vo.winning_odds)"}
                                {$vo.winning_odds}
                                {else /}
                                --
                                {/if}
                            </li>
                        </ul>
                    </li>
                    {/foreach}
                </ul>
            </div>
            <!-- /开奖的奖池获奖人数信息 -->

            <!-- 开奖结果  -->
            <div class="history-result">
                <h2 class="satel-news-tit">{:lang('history_results')}</h2>
                <ul class="hr-list">
                    {foreach name="drawList" item="vo" key="k"}
                    <li class="hr-item clearfix" _href="{$lang}/{$game.slug}/results/{$vo.draw.dateSlug}">
                        <div class="hr-item-l satel-item-ball">
                            {if condition="!empty($vo.draw)"}
                            <div class="history-time">{$vo.draw.date}</div>
                            <ul class="qpc-num-list  satel-ball clearfix">

                                {foreach name="$vo.draw.numbers" item="num"}
                                <li class="satel-baller">{$num}</li>
                                {/foreach}
                                {if condition="!empty($vo.draw.extranumbers)"}
                                {foreach name="$vo.draw.extranumbers" item="num"}
                                <li class="satel-baller extra-num ">{$num}</li>
                                {/foreach}
                                {/if}
                                {if condition="!empty($vo.draw.bonusnumbers)"}
                                {foreach name="$vo.draw.bonusnumbers" item="num"}
                                <li class="satel-baller bonus-num ">{$num}</li>
                                {/foreach}
                                {/if}
                                {if condition="!empty($vo.draw.refundnumbers)"}
                                {foreach name="$vo.draw.refundnumbers" item="num"}
                                <li class="satel-baller">{$num}</li>
                                {/foreach}
                                {/if}
                            </ul>
                            {/if}
                        </div>
                        <div class="hr-item-r">
                            <div class="history-jac">{:lang('jackpot')}: <span> €{$vo.draw.jackpot}</span></div>
                            <a href="{$lang}/{$game.slug}/results/{$vo.draw.dateSlug}"><div class="button-bt3 js-history-draw" >Prize breakdown</div></a>
                        </div>
                    </li>
                    {/foreach}

                </ul>
            </div>
            <!-- /开奖结果  -->
        </div>
        <!-- 右边的推广 -->
        <div class="prize-num-r">
            <!-- 默认的广告图 -->
            <div class="help-user-buy">
                <img src="/__PUBLIC__images/satellite/add-b.png" alt="">
            </div>
            <!-- 推荐彩种1 /4-->
            {foreach name="recommendGame" item="vo" key="k"}
            <div class="recom-lotto-box">
                <div class="rexom-lotto-head  {$vo.bg_color}-bg">
                    <div class="lotto-icon">
                        <img src="{$vo.logo}" alt="{$vo.name}">
                    </div>
                </div>
                <div class="recom-next">
                    <div class="recom-next-jac">{:lang('next_jackpot')}</div>
                    <div class="recom-jac-num">€{$vo.jackpot}</div>
                    {if condition="$isPlayContry === true && in_array($vo.slug,[
                        'usa-powerball',
                        'eurojackpot',
                        'mega-millions',
                        'super-ena-lotto',
                        'california-super-lotto',
                        'euromillions',
                        'germany-lotto',
                        'la-primitiva',
                        'el-gordo',
                        'cash4life',
                        'finland-lotto',
                        'canada-lotto-649',
                        'new-york-lotto',
                        'poland-lotto',
                        'irish-lotto',
                        'norway-lotto',
                        'mega-sena',
                        'bonoloto',
                        'austria-lotto',
                    ])"}
                    <a href="https://www.multilotto.com/{$langHead}/{$vo.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {else /}
                    <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {/if}
                </div>
            </div>
            {/foreach}
        </div>
    </div>

    <!-- prize breakdown  -->

    <!-- /prize breakdown  -->


    <!-- How to Check Lotto Results  start -->
    {if condition="!empty($pageContent)"}
    <div class="satel-lotto-check">
        <h2 class="satel-check-tit">{:lang('how_to_check_lotto_results')}</h2>
        <div class="satel-check-cont clearfix">
            {$pageContent.content}
        </div>
    </div>
    {/if}
    <!-- How to Check Lotto Results  end -->

    <!-- Lotto News  start -->
    <div class="satel-lotto-news">
        <h2 class="satel-news-tit">{:lang('lotto_news')}
            <div class="snt-more js-more-article">
                <a href="{$lang}/articles" style="color: black">
                    {:lang('more')}
                    <div class="snt-more-icon"></div>
                </a>
            </div>
        </h2>
        <div class="satel-news-cont">
            <ul class="snc-list clearfix">
                {foreach name="article_home_page" item="vo"}
                <li class="snc-item clearfix">
                    <a href="{$lang}/articles/{$vo.slug}">
                        <div class="snc-item-img">
                            <img src="{$vo.cover}"  alt="">
                        </div>
                        <div class="snc-item-detail">
                            <p>{$vo.title}</p>
                            <div class="lnd-signs  snc-item-signs clearfix">
                                <ul>
                                    {if condition="!empty($vo.gameTypes)"}
                                    {foreach name="vo.gameTypes" item="v"}
                                    <li>{$v}</li>
                                    {/foreach}
                                    {/if}
                                </ul>
                            </div>
                        </div>
                    </a>
                </li>
                {/foreach}
            </ul>
        </div>
    </div>
    <!-- Lotto News  end -->
</div>
<script>
    $(function () {
        $(".js-history-draw").on('click', function () {
            var _href = $(this).attr('_href');
            window.location.href = _href;
        })
    })
    var d1=$('.js-days span').eq(0).html(),
        d2=$('.js-days span').eq(1).html(),
        h1=$('.js-hours span').eq(0).html(),
        h2=$('.js-hours span').eq(1).html(),
        m1=$('.js-minutes span').eq(0).html(),
        m2=$('.js-minutes span').eq(1).html(),
        s1=$('.js-seconds span').eq(0).html(),
        s2=$('.js-seconds span').eq(1).html();
    $(function () {
        if(d1){
            setInterval(changeTime,1000);
        }
    });
    function changeTime() {
        s2 = s2 - 1;
        if(s2 < 0) {
            s2 = 9;
            s1 = s1 - 1;
            if(s1 < 0){
                s1 = 5;
                m2 = m2-1;
                if(m2 < 0) {
                    m2 = 9;
                    m1 = m1 -1;
                    if(m1 < 0) {
                        m1 = 5;
                        h2 = h2-1;
                        if(h2 < 0) {
                            h2 = 9;
                            h1 = h1 - 1;
                        }
                    }
                }
            }
        }
        $('.js-days span').eq(0).html(d1);
        $('.js-days span').eq(1).html(d2);
        $('.js-hours span').eq(0).html(h1);
        $('.js-hours span').eq(1).html(h2);
        $('.js-minutes span').eq(0).html(m1);
        $('.js-minutes span').eq(1).html(m2);
        $('.js-seconds span').eq(0).html(s1);
        $('.js-seconds span').eq(1).html(s2);
    }
    $(function () {
        var lang = $("#js-lang").val();
        $(".js-more-article").on('click', function () {
            window.location.href = lang +'/articles';
        })
        $(".history-result .hr-item").on('click',function () {
            var href = $(this).attr("_href");
            window.location.href = href;
        })
    })
</script>
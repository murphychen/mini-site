<div class="satel-cont  prize-next-draw">
    <div class="prize-wrap clearfix">
        <!-- 左边的开奖信息 -->
        <div class="prize-num-l">
            <!-- 开奖的奖池获奖人数信息 -->
            <div class="prize-break">
                <h1 class="satel-news-tit">{$game.name }  {:lang('results_for') } {if condition="isset($draw.date)"} {$draw.date}{/if}</h1>
            </div>
            <div class="draw-time-cont">
                <!-- 开奖结果 -->
                <div class="satel-result-item satel-resulter powerball-bg">
                    <div class="satel-item-l">
                        <div class="lotto-icon">
                            <img src="{$game.logo}" alt="{$game.name}">
                        </div>
                    </div>
                    <div class="satel-item-r ">
                        <div class="satel-item-time">{if condition="isset($draw.date)"}{$draw.date}{/if}</div>
                        <div class="satel-item-ball">
                            <ul class="satel-ball clearfix">
                                {if condition="!empty($draw)"}
                                {foreach name="$draw.numbers" item="num"}
                                <li class="satel-baller">{$num}</li>
                                {/foreach}
                                {if condition="!empty($draw.extranumbers)"}
                                {foreach name="$draw.extranumbers" item="num"}
                                <li class="satel-baller extra-num ">{$num}</li>
                                {/foreach}
                                {/if}
                                {if condition="!empty($draw.bonusnumbers)"}
                                {foreach name="$draw.bonusnumbers" item="num"}
                                <li class="satel-baller bonus-num ">{$num}</li>
                                {/foreach}
                                {/if}

                                {if condition="!empty($draw.refundnumbers)"}
                                {foreach name="$draw.refundnumbers" item="num"}
                                <li class="satel-baller">{$num}</li>
                                {/foreach}
                                {/if}

                                {/if}
                            </ul>
                        </div>
                    </div>
                    <!-- 右边的按钮 -->
                    {if condition="!empty($draw)"}
                    <div class="result-but">
                        <a href="{$lang}/{$game.slug}/results/{$date}/pre"><div class="button-bt2">{:lang('previous')}</div></a>
                        <a href="{$lang}/{$game.slug}/results/{$date}/next"><div class="button-bt2">{:lang('next')}</div></a>
                    </div>
                    {/if}
                </div>

            </div>
            <!-- 开奖的奖池获奖人数信息 -->
            <div class="prize-break">
                <h2 class="satel-news-tit">{:lang('prize_breakdown')}</h2>
                <ul class="pb-tb">

                    <li class="pb-tb-tr clearfix  pb-tb-tit">
                        <ul class="pb-tr-list clearfix">
                            <!--<li class="pb-tr-item">{:lang('divisions')}</li>-->
                            <li class="pb-tr-item">{:lang('match')}</li>
                            <li class="pb-tr-item">{:lang('prize_money')}</li>
                            <li class="pb-tr-item">{:lang('winners')}</li>
                            <li class="pb-tr-item">{:lang('winning_odds')}</li>
                        </ul>
                    </li>

                    {foreach name="payouts" item="vo"}
                    <li class="pb-tb-tr clearfix">
                        <ul class="pb-tr-list  clearfix">
                            <!--<li class="pb-tr-item">{$vo.divisions}</li>-->
                            <li class="pb-tr-item">{$vo.match}</li>
                            <li class="pb-tr-item">
                            	{if condition="$vo.prize_money=='No winner'"}
                            	{$vo.prize_money}
                            	{else /}
                            	€{$vo.prize_money}
                            	{/if}
                            </li>
                            <li class="pb-tr-item">
                                {if condition="!is_null($vo.winners)"}
                                {$vo.winners}
                                {else /}
                                --
                                {/if}
                            </li>
                            <li class="pb-tr-item">
                                {if condition="!empty($vo.winning_odds)"}
                                {$vo.winning_odds}
                                {else /}
                                --
                                {/if}
                            </li>
                        </ul>
                    </li>
                    {/foreach}
                </ul>
            </div>
            <!-- next draw -->
            {if condition="$game.nextdrawtime.status === true"}
            <div class="prize-break">
                <h2 class="satel-news-tit">{:lang('next_draw')}</h2>
                <div class="article-recom-lotto clearfix">
                    <div class="count-time-cont">
                        <div class="ctc-tit">{:lang('new_drawing')}</div>
                        <div class="ctc-list">
                            <ul class="ctc-list-box clearfix">
                                <li class="ctc-item js-days">
                                    <span class="ctc-num-box">{$game.nextdrawtime.data.d1}</span>
                                    <span class="ctc-num-box">{$game.nextdrawtime.data.d2}</span>
                                    <div class="ctc-time">{:lang('days')}</div>
                                </li>
                                <li class="ctc-item js-hours">
                                    <span class="ctc-num-box">{$game.nextdrawtime.data.h1}</span>
                                    <span class="ctc-num-box">{$game.nextdrawtime.data.h2}</span>
                                    <div class="ctc-time">{:lang('hours')}</div>
                                </li>
                                <li class="ctc-item js-minutes">
                                    <span class="ctc-num-box">{$game.nextdrawtime.data.m1}</span>
                                    <span class="ctc-num-box">{$game.nextdrawtime.data.m2}</span>
                                    <div class="ctc-time">{:lang('minutes')}</div>
                                </li>
                                <li class="ctc-item js-seconds">
                                    <span class="ctc-num-box ctc-num-red">{$game.nextdrawtime.data.s1}</span>
                                    <span class="ctc-num-box ctc-num-red">{$game.nextdrawtime.data.s2}</span>
                                    <div class="ctc-time">{:lang('seconds')}</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="recom-next">
                        <div class="recom-next-jac">{:lang('next_jackpot')}</div>
                        <div class="recom-jac-num">€ {$game.jackpot}</div>
                        {if condition="$isPlayContry === true && in_array($game.slug,[
                            'usa-powerball',
                            'eurojackpot',
                            'mega-millions',
                            'super-ena-lotto',
                            'california-super-lotto',
                            'euromillions',
                            'germany-lotto',
                            'la-primitiva',
                            'el-gordo',
                            'cash4life',
                            'finland-lotto',
                            'canada-lotto-649',
                            'new-york-lotto',
                            'poland-lotto',
                            'irish-lotto',
                            'norway-lotto',
                            'mega-sena',
                            'bonoloto',
                            'austria-lotto',
                        ])"}
                        <a href="https://www.multilotto.com/{$langHead}/{$game.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                        {else /}
                        <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                        {/if}
                    </div>

                </div>
            </div>
            {/if}

        </div>
        <!-- 右边的推广 -->
        <div class="prize-num-r">
            <!-- 默认的广告图 -->
            <div class="help-user-buy">
                <img src="/__PUBLIC__images/satellite/add-b.png" alt="">
            </div>
            <!-- 推荐彩种1 /2-->
            {foreach name="recommendGame" item="vo" key="k"}
            {if condition="$k < 2"}
            <div class="recom-lotto-box">
                <div class="rexom-lotto-head  {$vo.bg_color}-bg">
                    <div class="lotto-icon">
                        <img src="{$vo.logo}" alt="{$vo.name}">
                    </div>
                </div>
                <div class="recom-next">
                    <div class="recom-next-jac">{:lang('next_jackpot')}</div>
                    <div class="recom-jac-num">€{$vo.jackpot}</div>
                    {if condition="$isPlayContry === true && in_array($vo.slug,[
                        'usa-powerball',
                        'eurojackpot',
                        'mega-millions',
                        'super-ena-lotto',
                        'california-super-lotto',
                        'euromillions',
                        'germany-lotto',
                        'la-primitiva',
                        'el-gordo',
                        'cash4life',
                        'finland-lotto',
                        'canada-lotto-649',
                        'new-york-lotto',
                        'poland-lotto',
                        'irish-lotto',
                        'norway-lotto',
                        'mega-sena',
                        'bonoloto',
                        'austria-lotto',
                    ])"}
                    <a href="https://www.multilotto.com/{$langHead}/{$vo.slug}?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {else /}
                    <a href="https://www.multilotto.com/{$langHead}/lotto?ml=cos3vame"><div class="button-bt1">{:lang('play_now')}</div></a>
                    {/if}
                </div>
            </div>
            {/if}
            {/foreach}
        </div>

    </div>
    <!-- How to Check Lotto Results  start -->
    {if condition="!empty($pageContent)"}
    <div class="satel-lotto-check">
        <h2 class="satel-check-tit">{:lang('how_to_check_lotto_results')}</h2>
        <div class="satel-check-cont clearfix">
            {$pageContent.content}
        </div>
    </div>
    {/if}
    <!-- How to Check Lotto Results  end -->
</div>
<script>


    var lang = $("#js-lang").val();
    var d1=$('.js-days span').eq(0).html(),
        d2=$('.js-days span').eq(1).html(),
        h1=$('.js-hours span').eq(0).html(),
        h2=$('.js-hours span').eq(1).html(),
        m1=$('.js-minutes span').eq(0).html(),
        m2=$('.js-minutes span').eq(1).html(),
        s1=$('.js-seconds span').eq(0).html(),
        s2=$('.js-seconds span').eq(1).html();
    $(function () {
        lang = $("#js-lang").val();
        if(d1){
            setInterval(changeTime,1000);
        }
    });
    function changeTime() {
        s2 = s2 - 1;
        if(s2 < 0) {
            s2 = 9;
            s1 = s1 - 1;
            if(s1 < 0){
                s1 = 5;
                m2 = m2-1;
                if(m2 < 0) {
                    m2 = 9;
                    m1 = m1 -1;
                    if(m1 < 0) {
                        m1 = 5;
                        h2 = h2-1;
                        if(h2 < 0) {
                            h2 = 9;
                            h1 = h1 - 1;
                        }
                    }
                }
            }
        }
        $('.js-days span').eq(0).html(d1);
        $('.js-days span').eq(1).html(d2);
        $('.js-hours span').eq(0).html(h1);
        $('.js-hours span').eq(1).html(h2);
        $('.js-minutes span').eq(0).html(m1);
        $('.js-minutes span').eq(1).html(m2);
        $('.js-seconds span').eq(0).html(s1);
        $('.js-seconds span').eq(1).html(s2);
    }
</script>
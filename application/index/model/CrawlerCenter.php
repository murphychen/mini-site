<?php
/**
 * 2019/6/14
 * @author zhutd
 */

namespace app\index\model;
use think\Db;
class CrawlerCenter
{
    const signkey = 'hdahawudhwmfnaw';
    const SUCCESS = 20000;
    const FAILED = 10000;
    const INVALID_DATA_TYPE = 10001;
    const SIGN_INVALID = 10002;
    const EMPTY_DATA = 10004;

    /**
     *
     * @param mixed
     * Author: murphy
     */
    public static function insertDraws($data)
    {
        $matchRes = self::getLottoids($data);
        $finishIds = [];
        
        if (!empty($matchRes['matchRows'])){
            foreach ($matchRes['matchRows'] as $k=>$v){
                $ispartition = $v['bonusnumbersdrawn'] > 0 ? 1: 0;
                $decoderes = self::decodeCodes($v['codes'],$ispartition);
                $insertDatas = [];
                $insertDatas['drawdate'] = $v['drawtime'];
                $insertDatas['localdrawdatetime'] = $v['localdrawdatetime'];
                $insertDatas['jackpotsize'] = $v['jackpot'];
                $insertDatas['jackpotcurrency'] = $v['currency'];
                $insertDatas['number_str'] = $decoderes['preArr'] > 0 ? implode('-', $decoderes['preArr']) : 'P-E-N-D-I-N-G';
                $insertDatas['bonusnumber_str'] = implode('-', $decoderes['bArr']);
                $insertDatas['extranumber_str'] = implode('-', $decoderes['exArr']);
                try{
                    $select_res = Db::name('games')->where(['batchstr' => $v['batchstr']])->select();
                    if (count($select_res) > 0) {
                        Db::name('games')->where(['batchstr' => $v['batchstr']])->update($insertDatas);
                        $finishIds[] = $v['id'];
                    } else {
                        $insertDatas['gameslug'] = $v['slug'];
                        $insertDatas['batchstr'] = $v['batchstr'];
                        $reseult = Db::name('games')->insert($insertDatas);
                        if ($reseult > 0) {
                            $finishIds[] = $v['id'];
                        }else{
                            $matchRes['failedIds'][] = $v['id'];
                        }
                    }
                } catch (\Exception $ex) {
                    $matchRes['failedIds'][] = $v['id'];
                }
            }
        }
        return [
                'failedIds' => $matchRes['failedIds'],
                'ignoreIds' => $matchRes['ignoreIds'],
                'finishIds' => $finishIds,
            ];
    }

    /**
     *
     * @param mixed
     * Author: murphy
     */
    private static function decodeCodes($codeStr, $partition = 0)
    {
        $codearr = explode('|', $codeStr);
        $preArr = explode(',',$codearr[0]);
        $exArr = [];
        $bArr = [];
        $aexArr = isset($codearr[1]) ? explode(",",$codearr[1]) : [];
        if ($partition === 1) {
            $bArr = $aexArr;
        }else{
            $exArr = $aexArr;
        }
        return [
            "preArr" => $preArr,
            "bArr" => $bArr,
            "exArr" => $exArr,
        ];
    }
    /**
     *
     * @param mixed
     * Author: murphy
     */
    public static function insertPayout($data)
    {
        $matchRes = self::getLottoids($data);
        $finishIds = [];
        if (!empty($matchRes['matchRows'])){
            foreach ($matchRes['matchRows'] as $k=>$v){
                $insertDatas = [];
                $insertDatas['probability'] = $v['odds'];
                $insertDatas['payout'] = $v['eurprize'];
                $insertDatas['payoutcurrency'] = $v['currency'];
                $insertDatas['divisions'] = $v['sortorder'];
                $insertDatas['match'] = $v['matchNum'];
                $insertDatas['winning_odds'] = '1:' . number_format($v['odds'], 0,'',' ');
                $insertDatas['prize_money'] = number_format($v['eurprize'],2,'.',' ');
                $insertDatas['winners'] = $v['winners'];
                try{
                    $select_res = Db::name('games_payouttables')->where(['batchstr' => $v['batchstr'], 'sortorder' => $v['sortorder']])->select();
                    if (count($select_res) > 0) {
                        Db::name('games_payouttables')->where(['batchstr' => $v['batchstr'], 'sortorder' => $v['sortorder']])->update($insertDatas);
                        $finishIds[] = $v['id'];
                    } else {
                        $insertDatas['drawid'] = -1000;
                        $insertDatas['sortorder'] = $v['sortorder'];
                        $insertDatas['batchstr'] = $v['batchstr'];
                        $reseult = Db::name('games_payouttables')->insert($insertDatas);
                        if ($reseult > 0) {
                            $finishIds[] = $v['id'];
                        }else{
                            $matchRes['failedIds'][] = $v['id'];
                        }
                    }
                    
                } catch (\Exception $ex) {
                    $matchRes['failedIds'][] = $v['id'];
                }
            }
        }
        return [
            'failedIds' => $matchRes['failedIds'],
            'ignoreIds' => $matchRes['ignoreIds'],
            'finishIds' => $finishIds,
        ];
    }

    /**
     *
     * @param mixed
     * Author: murphy
     */
    private static function getLottoids($data)
    {
        $slugs = [];
        $mapData = [
            'ignoreIds' => [],
            'matchRows' => [],
            'failedIds' => [],
        ];
        $slugArr = $whereArr = $tempArr = [];
        foreach ($data as $k=>$v) {
            $tempArr[] = str_replace("_","-",$v['slug']);
        }
        $whereArr['slug'] = ['in', $tempArr];
        $result = Db::name('game_types')->field('slug,gametypeid,currency,numbersdrawn,extranumbersdrawn,bonusnumbersdrawn')
        ->where($whereArr)
        ->select();
        foreach ($result as $row) {
            $slugArr[$row['slug']]['currency'] = $row['currency'];
            $slugArr[$row['slug']]['numbersdrawn'] = $row['numbersdrawn'];
            $slugArr[$row['slug']]['extranumbersdrawn'] = $row['extranumbersdrawn'];
            $slugArr[$row['slug']]['bonusnumbersdrawn'] = $row['bonusnumbersdrawn'];
        }
        foreach ($data as $k=>$v) {
            $slug = str_replace("_","-",$v['slug']);
            if (isset($slugArr[$slug])) {
                $v['slug'] = $slug;
                $v['currency'] = $slugArr[$slug]['currency'];
                $v['numbersdrawn'] = $slugArr[$slug]['numbersdrawn'];
                $v['extranumbersdrawn'] = $slugArr[$slug]['extranumbersdrawn'];
                $v['bonusnumbersdrawn'] = $slugArr[$slug]['bonusnumbersdrawn'];
                $mapData['matchRows'][] = $v;
            }else{
                $mapData['ignoreIds'][] = $v['id'];
            }
        }
        return $mapData;
    }
}